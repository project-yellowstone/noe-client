FROM node:10-alpine

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# building the code for production
RUN npm install --only=production

# build code for production
RUN npm run build

# Bundle app source
COPY . .

# TODO: HEALTHCHECK ?

ENV NODE_ENV production
ENV PORT=8080

EXPOSE 8080
CMD ["npm", "start:prod"]

# Ensure that the programm will start as non root user
USER node
