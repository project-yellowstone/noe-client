import styled from 'styled-components';

export default styled.header`
  border-radius: var(--radius);
  color: var(--color-secondary-text);
  text-transform: uppercase;
  font-weight: 700;
`;
