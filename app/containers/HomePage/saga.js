import { all, select, call, put, takeLatest } from 'redux-saga/effects';

import api from 'utils/api';
import { addNotification } from 'containers/Alerts/actions';
import {
  // NewsFeedYounger
  newsFeedYoungerLoadOk,
  newsFeedYoungerLoadError,
  // NewsFeedOlder
  newsFeedOlderLoadOk,
  newsFeedOlderLoadError,
} from './actions';
import {
  // NewsFeed
  makeSelectNewsFeedFrom,
  makeSelectNewsFeedTo,
} from './selectors';
import {
  // NEWSFEED_YOUNGER
  NEWSFEED_YOUNGER_LOADING,
  // NEWSFEED_OLDER
  NEWSFEED_OLDER_LOADING,
} from './constants';

export function* loadNewsFeedYounger() {
  yield loadNewsFeed(
    sortNewsFeedDataAsc,
    true,
    newsFeedYoungerLoadOk,
    newsFeedYoungerLoadError,
  );
}

export function* loadNewsFeedOlder() {
  yield loadNewsFeed(
    sortNewsFeedDataDesc,
    false,
    newsFeedOlderLoadOk,
    newsFeedOlderLoadError,
  );
}

export function* loadNewsFeed(sortFunc, isFetchNewer, putFuncOk, putFuncErr) {
  const amountToLoad = 5;
  const { loadFrom, loadTo } = yield all({
    loadFrom: select(makeSelectNewsFeedFrom()),
    loadTo: select(makeSelectNewsFeedTo()),
  });

  const computerLoadFrom = loadFrom || new Date(); // default is now
  let computerLoadTo = loadTo || new Date(); // default is null

  try {
    // send the data requests
    const models = yield all({
      iam: call(
        loadIam,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      persons: call(
        loadPersons,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      phylogenetic: call(
        loadPhylogenetic,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      projects: call(
        loadProjects,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      resources: call(
        loadResources,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      userGroups: call(
        loadUserGroups,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      users: call(
        loadUsers,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
      warehouses: call(
        loadWarehouses,
        isFetchNewer ? undefined : computerLoadTo,
        isFetchNewer ? computerLoadFrom : undefined,
      ),
    });

    const formatedModels = [];
    Object.keys(models).forEach(modelKey => {
      const { total, data: modelData } = models[modelKey];

      // if there is no data or if the field "total" is non present
      if (!(parseInt(total, 10) > 0)) {
        return;
      }

      modelData.forEach(modelContent => {
        const obj = {};
        obj[modelKey] = modelContent;
        formatedModels.push(obj);
      });
    });

    let properDatas = [];
    // if the result is non-empty
    if (formatedModels.length > 0) {
      formatedModels.sort(sortFunc); // sort

      // cut the array to the desired size
      properDatas = formatedModels.slice(0, amountToLoad);

      computerLoadTo = new Date(
        properDatas[properDatas.length - 1][
          Object.keys(properDatas[properDatas.length - 1])[0]
        ].updatedAt,
      );
    }

    yield put(putFuncOk(properDatas, computerLoadFrom, computerLoadTo));
  } catch (err) {
    yield put(putFuncErr(err));
    yield put(addNotification(err.message));
  }
}

/*
 * Sort from the smallest to the highest
 */
function sortNewsFeedDataAsc(a, b) {
  const aValue = a[Object.keys(a)[0]].updatedAt;
  const bValue = b[Object.keys(b)[0]].updatedAt;
  if (aValue < bValue) return -1;
  if (aValue > bValue) return 1;
  return 0;
}

/*
 * Sort from the highest to the smallest
 */
function sortNewsFeedDataDesc(a, b) {
  const aValue = a[Object.keys(a)[0]].updatedAt;
  const bValue = b[Object.keys(b)[0]].updatedAt;
  if (aValue < bValue) return 1;
  if (aValue > bValue) return -1;
  return 0;
}

export function* loadIam(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.iamService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        $select: ['name', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadPersons(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.personsService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadPhylogenetic(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.phylogeneticService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        $populate: ['parent'],
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadProjects(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.projectsService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadResources(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.resourcesService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        $populate: ['species'],
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadUserGroups(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.userGroupsService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadUsers(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.usersService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

export function* loadWarehouses(lt, gt, limit) {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.warehousesService.find({
      query: {
        updatedAt: { $lt: lt, $gt: gt },
        $limit: limit,
        // $select: ['firstname', 'lastname', 'updatedAt'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    return res;
  } catch (err) {
    throw err;
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* homePageData() {
  yield takeLatest(NEWSFEED_YOUNGER_LOADING, loadNewsFeedYounger);
  yield takeLatest(NEWSFEED_OLDER_LOADING, loadNewsFeedOlder);
}
