/*
 * AccountPage actions
 *
 * Actions change things in your application. Since this boilerplate uses a uni-directional data flow, specifically redux, we have these actions which are the only way your application interacts with your application state. This guarantees that your state is up to date and nobody messes it up weirdly somewhere.
 */
import {
  // NEWSFEED_YOUNGER
  NEWSFEED_YOUNGER_LOADING,
  NEWSFEED_YOUNGER_LOADING_OK,
  NEWSFEED_YOUNGER_LOADING_ERROR,
  // NEWSFEED_OLDER
  NEWSFEED_OLDER_LOADING,
  NEWSFEED_OLDER_LOADING_OK,
  NEWSFEED_OLDER_LOADING_ERROR,
} from './constants';

// NEWSFEED_YOUNGER

export function newsFeedYoungerLoad() {
  return {
    type: NEWSFEED_YOUNGER_LOADING,
  };
}

export function newsFeedYoungerLoadOk(data, from, to) {
  return {
    type: NEWSFEED_YOUNGER_LOADING_OK,
    data,
    from,
    to,
  };
}

export function newsFeedYoungerLoadError(error) {
  return {
    type: NEWSFEED_YOUNGER_LOADING_ERROR,
    error,
  };
}

// NEWSFEED_OLDER

export function newsFeedOlderLoad() {
  return {
    type: NEWSFEED_OLDER_LOADING,
  };
}

export function newsFeedOlderLoadOk(data, from, to) {
  return {
    type: NEWSFEED_OLDER_LOADING_OK,
    data,
    from,
    to,
  };
}

export function newsFeedOlderLoadError(error) {
  return {
    type: NEWSFEED_OLDER_LOADING_ERROR,
    error,
  };
}
