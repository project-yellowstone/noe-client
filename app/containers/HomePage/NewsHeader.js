import styled from 'styled-components';

export default styled.header`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  font-size: 0.75rem;
  margin-bottom: 0.75rem;

  > :not(:last-child) {
    margin-right: 0.5rem;
  }
`;
