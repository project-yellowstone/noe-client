import styled from 'styled-components';

export default styled.header`
  color: var(--color-secondary-text-light);
`;
