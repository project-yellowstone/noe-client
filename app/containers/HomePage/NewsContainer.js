import styled from 'styled-components';

export default styled.article`
  border-radius: var(--radius);
  border: 1px solid var(--color-primary);
  padding: 0.75rem;

  &:not(last-of-type) {
    margin-bottom: 1.5rem;
  }
`;
