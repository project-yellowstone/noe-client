/*
 * HomePage recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';

import {
  // NEWSFEED_YOUNGER
  NEWSFEED_YOUNGER_LOADING,
  NEWSFEED_YOUNGER_LOADING_OK,
  NEWSFEED_YOUNGER_LOADING_ERROR,
  // NEWSFEED_OLDER
  NEWSFEED_OLDER_LOADING,
  NEWSFEED_OLDER_LOADING_OK,
  NEWSFEED_OLDER_LOADING_ERROR,
  // // Iam
  // NEWSFEED_IAM_LOADING,
  // NEWSFEED_IAM_LOADING_OK,
  // NEWSFEED_IAM_LOADING_ERROR,
  // // Persons
  // NEWSFEED_PERSONS_LOADING,
  // NEWSFEED_PERSONS_LOADING_OK,
  // NEWSFEED_PERSONS_LOADING_ERROR,
  // // Phylogenetic
  // NEWSFEED_PHYLOGENETIC_LOADING,
  // NEWSFEED_PHYLOGENETIC_LOADING_OK,
  // NEWSFEED_PHYLOGENETIC_LOADING_ERROR,
  // // Projects
  // NEWSFEED_PROJECTS_LOADING,
  // NEWSFEED_PROJECTS_LOADING_OK,
  // NEWSFEED_PROJECTS_LOADING_ERROR,
  // // Resources
  // NEWSFEED_RESOURCES_LOADING,
  // NEWSFEED_RESOURCES_LOADING_OK,
  // NEWSFEED_RESOURCES_LOADING_ERROR,
  // // UserGroups
  // NEWSFEED_USERGROUPS_LOADING,
  // NEWSFEED_USERGROUPS_LOADING_OK,
  // NEWSFEED_USERGROUPS_LOADING_ERROR,
  // // Users
  // NEWSFEED_USERS_LOADING,
  // NEWSFEED_USERS_LOADING_OK,
  // NEWSFEED_USERS_LOADING_ERROR,
  // // Warehouses
  // NEWSFEED_WAREHOUSES_LOADING,
  // NEWSFEED_WAREHOUSES_LOADING_OK,
  // NEWSFEED_WAREHOUSES_LOADING_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
  newsFeed: {
    from: undefined,
    to: undefined,
    fetchs: {
      younger: {
        loading: false,
        error: false,
      },
      older: {
        loading: false,
        error: false,
      },
    },
    data: [],
    // models: {
    //   // Iam
    //   iam: [],
    //   iamLoading: false,
    //   iamError: false,
    //   // Persons
    //   persons: [],
    //   personsLoading: false,
    //   personsError: false,
    //   // Phylogenetic
    //   phylogenetic: [],
    //   phylogeneticLoading: false,
    //   phylogeneticError: false,
    //   // Projects
    //   projects: [],
    //   projectsLoading: false,
    //   projectsError: false,
    //   // Resources
    //   resources: [],
    //   resourcesLoading: false,
    //   resourcesError: false,
    //   // UserGroups
    //   userGroups: [],
    //   userGroupsLoading: false,
    //   userGroupsError: false,
    //   // Users
    //   users: [],
    //   usersLoading: false,
    //   usersError: false,
    //   // Warehouses
    //   warehouses: [],
    //   warehousesLoading: false,
    //   warehousesError: false,
    // },
  },
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // NEWSFEED_YOUNGER
      case NEWSFEED_YOUNGER_LOADING:
        draft.newsFeed.fetchs.younger.loading = true;
        draft.newsFeed.fetchs.younger.error = false;
        break;

      case NEWSFEED_YOUNGER_LOADING_OK:
        draft.newsFeed.fetchs.younger.loading = false;
        draft.newsFeed.fetchs.younger.error = false;
        Object.keys(action.data).forEach(data =>
          draft.newsFeed.data.unshift(action.data[data]),
        );
        draft.newsFeed.from = action.from;
        draft.newsFeed.to = action.to;
        break;

      case NEWSFEED_YOUNGER_LOADING_ERROR:
        draft.newsFeed.fetchs.younger.loading = false;
        draft.newsFeed.fetchs.younger.error = action.error;
        break;

      // NEWSFEED_OLDER
      case NEWSFEED_OLDER_LOADING:
        draft.newsFeed.fetchs.older.loading = true;
        draft.newsFeed.fetchs.older.error = false;
        break;

      case NEWSFEED_OLDER_LOADING_OK:
        draft.newsFeed.fetchs.older.loading = false;
        draft.newsFeed.fetchs.older.error = false;
        Object.keys(action.data).forEach(data =>
          draft.newsFeed.data.push(action.data[data]),
        );
        draft.newsFeed.from = action.from;
        draft.newsFeed.to = action.to;
        break;

      case NEWSFEED_OLDER_LOADING_ERROR:
        draft.newsFeed.fetchs.older.loading = false;
        draft.newsFeed.fetchs.older.error = action.error;
        break;
    }
  });

export default homeReducer;
