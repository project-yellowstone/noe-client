/*
 * HomePage constants
 *
 * Each action has a corresponding type, which the reducer knows and picks up on. To avoid weird typos between the reducer and the actions, we save them as constants here. We prefix them with 'yourproject/YourComponent' so we avoid reducers accidentally picking up actions they shouldn't.
 */
// NEWSFEED_YOUNGER
export const NEWSFEED_YOUNGER_LOADING = 'app/HomePage/NEWSFEED_YOUNGER_LOADING';
export const NEWSFEED_YOUNGER_LOADING_OK =
  'app/HomePage/NEWSFEED_YOUNGER_LOADING_OK';
export const NEWSFEED_YOUNGER_LOADING_ERROR =
  'app/HomePage/NEWSFEED_YOUNGER_LOADING_ERROR';

// NEWSFEED_OLDER
export const NEWSFEED_OLDER_LOADING = 'app/HomePage/NEWSFEED_OLDER_LOADING';
export const NEWSFEED_OLDER_LOADING_OK =
  'app/HomePage/NEWSFEED_OLDER_LOADING_OK';
export const NEWSFEED_OLDER_LOADING_ERROR =
  'app/HomePage/NEWSFEED_OLDER_LOADING_ERROR';
