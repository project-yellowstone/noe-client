/*
 * ThemeProvider
 */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import * as moment from 'moment/moment';

import AbstractComponent from 'components/AbstractComponent';
import messages from './messages';
import Container from './NewsContainer';
import Header from './NewsHeader';
import Type from './NewsType';
import Date from './NewsDate';

export class NewsElement extends AbstractComponent {
  safeRender() {
    const { news } = this.props;

    const modelId = Object.keys(news)[0];
    const entity = news[modelId];

    const title = this.getTypeFromNews(modelId);
    const description = this.getDescriptionFromNews(modelId, entity);

    return (
      <Container>
        <Header>
          <Type>{title}</Type>
          <Date>{moment.default(entity.updatedAt).fromNow()}</Date>
        </Header>
        <p>{description}</p>
      </Container>
    );
  }

  getDescriptionFromNews(modelId, entity) {
    switch (modelId) {
      case 'iam': {
        const name = entity.name; // eslint-disable-line prefer-destructuring
        return (
          <FormattedMessage
            {...messages.news.models.iam.description}
            values={{ name: <b>{name}</b> }}
          />
        );
      }

      case 'persons': {
        const name = `${entity.firstname} ${entity.lastname}`;
        return (
          <FormattedMessage
            {...messages.news.models.persons.description}
            values={{ name: <b>{name}</b> }}
          />
        );
      }

      case 'phylogenetic': {
        const name = entity.name; // eslint-disable-line prefer-destructuring
        const ancestorName = entity.parent ? entity.parent.name : 'none'; // eslint-disable-line prefer-destructuring
        const newStatusEntity = entity.status[entity.status.length - 1];

        if (!newStatusEntity) {
          return (
            <FormattedMessage
              {...messages.news.models.phylogenetic.description.default}
              values={{
                name: <b>{name}</b>,
                ancestorName,
              }}
            />
          );
        }

        const newStatus = newStatusEntity.type;
        const olderStatusEntity =
          entity.status.length > 1
            ? entity.status[entity.status.length - 2]
            : undefined;
        const olderStatus = olderStatusEntity ? olderStatusEntity.type : 'none';

        return (
          <FormattedMessage
            {...messages.news.models.phylogenetic.description.status}
            values={{
              name: <b>{name}</b>,
              ancestorName,
              newStatus: <b>{newStatus}</b>,
              olderStatus: <i>{olderStatus}</i>,
            }}
          />
        );
      }

      case 'projects': {
        const name = entity.name; // eslint-disable-line prefer-destructuring
        const newStatusEntity = entity.status[entity.status.length - 1];

        if (!newStatusEntity) {
          return (
            <FormattedMessage
              {...messages.news.models.projects.description.default}
              values={{
                name: <b>{name}</b>,
              }}
            />
          );
        }

        const newStatus = newStatusEntity.type;
        const olderStatusEntity =
          entity.status.length > 1
            ? entity.status[entity.status.length - 2]
            : undefined;
        const olderStatus = olderStatusEntity ? olderStatusEntity.type : 'none';

        return (
          <FormattedMessage
            {...messages.news.models.projects.description.status}
            values={{
              name: <b>{name}</b>,
              newStatus: <b>{newStatus}</b>,
              olderStatus: <i>{olderStatus}</i>,
            }}
          />
        );
      }

      case 'resources': {
        const type = entity.type; // eslint-disable-line prefer-destructuring
        const speciesName = entity.species ? entity.species.name : 'none'; // eslint-disable-line prefer-destructuring
        const newStatusEntity = entity.status[entity.status.length - 1];
        const newStatus = newStatusEntity.type;
        const olderStatusEntity =
          entity.status.length > 1
            ? entity.status[entity.status.length - 2]
            : undefined;
        const olderStatus = olderStatusEntity ? olderStatusEntity.type : 'none';

        return (
          <FormattedMessage
            {...messages.news.models.resources.description.status}
            values={{
              type: <b>{type}</b>,
              speciesName,
              newStatus: <b>{newStatus}</b>,
              olderStatus: <i>{olderStatus}</i>,
            }}
          />
        );
      }

      case 'userGroups': {
        const name = entity.name; // eslint-disable-line prefer-destructuring
        return (
          <FormattedMessage
            {...messages.news.models.userGroups.description}
            values={{ name: <b>{name}</b> }}
          />
        );
      }

      case 'users': {
        const name = entity.email;
        return (
          <FormattedMessage
            {...messages.news.models.users.description}
            values={{ name: <b>{name}</b> }}
          />
        );
      }

      case 'warehouses': {
        const name = entity.description;
        const newStatusEntity = entity.status[entity.status.length - 1];
        const newStatus = newStatusEntity.type;
        const olderStatusEntity =
          entity.status.length > 1
            ? entity.status[entity.status.length - 2]
            : undefined;
        const olderStatus = olderStatusEntity ? olderStatusEntity.type : 'none';

        return (
          <FormattedMessage
            {...messages.news.models.warehouses.description.status}
            values={{
              name: <b>{name}</b>,
              newStatus: <b>{newStatus}</b>,
              olderStatus: <i>{olderStatus}</i>,
            }}
          />
        );
      }

      default:
        return undefined;
    }
  }

  getTypeFromNews(modelId) {
    switch (modelId) {
      case 'iam': {
        return <FormattedMessage {...messages.news.models.iam.title} />;
      }

      case 'persons': {
        return <FormattedMessage {...messages.news.models.persons.title} />;
      }

      case 'phylogenetic': {
        return (
          <FormattedMessage {...messages.news.models.phylogenetic.title} />
        );
      }

      case 'projects': {
        return <FormattedMessage {...messages.news.models.projects.title} />;
      }

      case 'resources': {
        return <FormattedMessage {...messages.news.models.resources.title} />;
      }

      case 'userGroups': {
        return <FormattedMessage {...messages.news.models.userGroups.title} />;
      }

      case 'users': {
        return <FormattedMessage {...messages.news.models.users.title} />;
      }

      case 'warehouses': {
        return <FormattedMessage {...messages.news.models.warehouses.title} />;
      }

      default:
        return undefined;
    }
  }
}

NewsElement.propTypes = {
  news: PropTypes.object,
};

export default NewsElement;
