/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.home || initialState;

// NewsFeed

const makeSelectNewsFeedYoungerLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.fetchs.younger.loading,
  );

const makeSelectNewsFeedYoungerError = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.fetchs.younger.error,
  );

const makeSelectNewsFeedOlderLoading = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.fetchs.older.loading,
  );

const makeSelectNewsFeedOlderError = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.fetchs.older.error,
  );

const makeSelectNewsFeedFrom = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.from,
  );

const makeSelectNewsFeedTo = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.to,
  );

const makeSelectNewsFeedData = () =>
  createSelector(
    selectHome,
    homeState => homeState.newsFeed.data,
  );

export {
  selectHome,
  // NewsFeed
  makeSelectNewsFeedYoungerLoading,
  makeSelectNewsFeedYoungerError,
  makeSelectNewsFeedOlderLoading,
  makeSelectNewsFeedOlderError,
  makeSelectNewsFeedFrom,
  makeSelectNewsFeedTo,
  makeSelectNewsFeedData,
};
