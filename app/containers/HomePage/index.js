import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import AbstractComponent from 'components/AbstractComponent';
import PageContainer from 'components/PageContainer';
import Button from 'components/Button';
import { newsFeedYoungerLoad, newsFeedOlderLoad } from './actions';
import {
  makeSelectNewsFeedYoungerLoading,
  makeSelectNewsFeedYoungerError,
  makeSelectNewsFeedOlderLoading,
  makeSelectNewsFeedOlderError,
  makeSelectNewsFeedData,
} from './selectors';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';
import News from './News';

/* eslint-disable react/prefer-stateless-function */
export class HomePage extends AbstractComponent {
  componentDidMount() {
    const {
      dispatchNewsFeedYoungerLoad,
      dispatchNewsFeedOlderLoad,
    } = this.props;

    dispatchNewsFeedYoungerLoad();
    dispatchNewsFeedOlderLoad();
  }

  safeRender() {
    const {
      // Dispatch
      dispatchNewsFeedYoungerLoad,
      dispatchNewsFeedOlderLoad,
      // NewsFeed
      newsFeedYoungerLoading,
      // newsFeedYoungerError,
      newsFeedOlderLoading,
      // newsFeedOlderError,
      newsFeedData,
    } = this.props;

    const newsList = newsFeedData.map(news => (
      <News
        key={news[Object.keys(news)[0]]._id} // eslint-disable-line no-underscore-dangle
        news={news}
      />
    ));

    return (
      <PageContainer title={messages.header} description={messages.description}>
        <Button
          loading={newsFeedYoungerLoading}
          onClick={dispatchNewsFeedYoungerLoad}
        >
          <FormattedMessage {...messages.news.loadMore} />
        </Button>
        <div>{newsList}</div>
        <Button
          loading={newsFeedOlderLoading}
          onClick={dispatchNewsFeedOlderLoad}
        >
          <FormattedMessage {...messages.news.loadMore} />
        </Button>
      </PageContainer>
    );
  }
}

HomePage.propTypes = {
  // Dispatch
  dispatchNewsFeedYoungerLoad: PropTypes.func,
  dispatchNewsFeedOlderLoad: PropTypes.func,
  // NewsFeed
  newsFeedYoungerLoading: PropTypes.bool,
  newsFeedYoungerError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  newsFeedOlderLoading: PropTypes.bool,
  newsFeedOlderError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  newsFeedData: PropTypes.array,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchNewsFeedYoungerLoad: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(newsFeedYoungerLoad());
    },
    dispatchNewsFeedOlderLoad: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(newsFeedOlderLoad());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  newsFeedYoungerLoading: makeSelectNewsFeedYoungerLoading(),
  newsFeedYoungerError: makeSelectNewsFeedYoungerError(),
  newsFeedOlderLoading: makeSelectNewsFeedOlderLoading(),
  newsFeedOlderError: makeSelectNewsFeedOlderError(),
  newsFeedData: makeSelectNewsFeedData(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
