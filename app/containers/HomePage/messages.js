/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.HomePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'News feed',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Site home page.',
  },
  news: {
    loadMore: {
      id: `${scope}.news.loadMore`,
      defaultMessage: 'Load more',
    },
    models: {
      iam: {
        title: {
          id: `${scope}.news.models.iam.title`,
          defaultMessage: 'IAM',
        },
        description: {
          id: `${scope}.news.models.iam.description`,
          defaultMessage: 'Update of {name}.',
        },
      },
      persons: {
        title: {
          id: `${scope}.news.models.persons.title`,
          defaultMessage: 'Person',
        },
        description: {
          id: `${scope}.news.models.persons.description`,
          defaultMessage: 'Update of {name}.',
        },
      },
      phylogenetic: {
        title: {
          id: `${scope}.news.models.phylogenetic.title`,
          defaultMessage: 'Phylogenetic',
        },
        description: {
          default: {
            id: `${scope}.news.models.resources.description.default`,
            defaultMessage: "The {name}, {ancestorName}'s child, is updated.",
          },
          status: {
            id: `${scope}.news.models.resources.description.status`,
            defaultMessage:
              "The {name}, {ancestorName}'s child, is now {newStatus} (was {olderStatus}).",
          },
        },
      },
      projects: {
        title: {
          id: `${scope}.news.models.projects.title`,
          defaultMessage: 'Project',
        },
        description: {
          default: {
            id: `${scope}.news.models.projects.description.default`,
            defaultMessage: 'Update of {name}.',
          },
          status: {
            id: `${scope}.news.models.projects.description.status`,
            defaultMessage:
              '{name} has started a step {newStatus} (was {olderStatus}).',
          },
        },
      },
      resources: {
        title: {
          id: `${scope}.news.models.resources.title`,
          defaultMessage: 'Resource',
        },
        description: {
          default: {
            id: `${scope}.news.models.resources.description.default`,
            defaultMessage: 'Update of {name}.',
          },
          status: {
            id: `${scope}.news.models.resources.description.status`,
            defaultMessage:
              "A {speciesName}'s {type} is {newStatus} (was {olderStatus}).",
          },
        },
      },
      userGroups: {
        title: {
          id: `${scope}.news.models.userGroups.title`,
          defaultMessage: 'User group',
        },
        description: {
          id: `${scope}.news.models.userGroups.description`,
          defaultMessage: 'Update of {name}.',
        },
      },
      users: {
        title: {
          id: `${scope}.news.models.users.title`,
          defaultMessage: 'User',
        },
        description: {
          id: `${scope}.news.models.users.description`,
          defaultMessage: 'Update of {name}.',
        },
      },
      warehouses: {
        title: {
          id: `${scope}.news.models.warehouses.title`,
          defaultMessage: 'Warehouse',
        },
        description: {
          default: {
            id: `${scope}.news.models.warehouses.description.default`,
            defaultMessage: 'Update of {name}.',
          },
          status: {
            id: `${scope}.news.models.warehouses.description.status`,
            defaultMessage: '{name} is now {newStatus} (was {olderStatus}).',
          },
        },
      },
    },
  },
});
