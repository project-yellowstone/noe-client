/*
 * ThemeProvider
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import AbstractComponent from 'components/AbstractComponent';
import { themeStyles } from 'themes';
import { makeSelectTheme } from './selectors';

export class ThemeProvider extends AbstractComponent {
  safeRender() {
    const { theme, children } = this.props;

    return (
      <div style={themeStyles[theme]}>{React.Children.only(children)}</div>
    );
  }
}

ThemeProvider.propTypes = {
  theme: PropTypes.string,
  children: PropTypes.element.isRequired,
};

const mapStateToProps = createStructuredSelector({
  theme: makeSelectTheme(),
});

export default connect(mapStateToProps)(ThemeProvider);
