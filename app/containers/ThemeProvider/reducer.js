/*
 * ThemeProvider recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';

import { DEFAULT_THEME } from 'themes';
import { CHANGE_THEME } from './constants';

export const initialState = {
  theme: DEFAULT_THEME,
};

/* eslint-disable default-case, no-param-reassign */
const themeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // CHANGE_THEME
      case CHANGE_THEME:
        draft.theme = action.theme;
        break;
    }
  });

export default themeReducer;
