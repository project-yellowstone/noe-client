import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the themeToggle state domain
 */
const selectTheme = state => state.theme || initialState;

/**
 * Select the theme
 */
const makeSelectTheme = () =>
  createSelector(
    selectTheme,
    themeState => themeState.theme,
  );

export { selectTheme, makeSelectTheme };
