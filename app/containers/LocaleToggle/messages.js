/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.LocaleToggle';

export default defineMessages({
  label: {
    id: `${scope}.label`,
    defaultMessage: 'Language',
  },
  en: {
    id: `${scope}.en`,
    defaultMessage: 'English',
  },
  de: {
    id: `${scope}.fr`,
    defaultMessage: 'French',
  },
});
