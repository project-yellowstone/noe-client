/*
 *
 * LanguageToggle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AbstractComponent from 'components/AbstractComponent';
import Toggle from 'components/Toggle';
import { appLocales } from 'i18n';
import { changeLocale } from 'containers/LanguageProvider/actions';
import { makeSelectLocale } from 'containers/LanguageProvider/selectors';
import Wrapper from './Wrapper';
import messages from './messages';

export class LocaleToggle extends AbstractComponent {
  // eslint-disable-line react/prefer-stateless-function
  safeRender() {
    return (
      <Wrapper>
        <Toggle
          id="localeToogle"
          value={this.props.locale}
          values={appLocales}
          messages={messages}
          name={messages.label}
          onToggle={this.props.onLocaleToggle}
        />
      </Wrapper>
    );
  }
}

LocaleToggle.propTypes = {
  onLocaleToggle: PropTypes.func,
  locale: PropTypes.string,
};

const mapStateToProps = createSelector(
  makeSelectLocale(),
  locale => ({
    locale,
  }),
);

export function mapDispatchToProps(dispatch) {
  return {
    onLocaleToggle: evt => dispatch(changeLocale(evt.target.value)),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocaleToggle);
