/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.login || initialState;

const makeSelectUser = () =>
  createSelector(
    selectLogin,
    loginState => loginState.userData.user,
  );

const makeSelectPerson = () =>
  createSelector(
    selectLogin,
    loginState => loginState.userData.person,
  );

const makeSelectLoading = () =>
  createSelector(
    selectLogin,
    loginState => loginState.loading,
  );

const makeSelectError = () =>
  createSelector(
    selectLogin,
    loginState => loginState.error,
  );

const makeSelectEmail = () =>
  createSelector(
    selectLogin,
    loginState => loginState.email,
  );

const makeSelectPassword = () =>
  createSelector(
    selectLogin,
    loginState => loginState.password,
  );

export {
  makeSelectUser,
  makeSelectPerson,
  selectLogin,
  makeSelectLoading,
  makeSelectError,
  makeSelectEmail,
  makeSelectPassword,
};
