import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import PageContainer from 'components/PageContainer';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import Form from 'components/Form';
import Input from 'components/Input';
import Button from 'components/Button';
import {
  makeSelectPerson,
  makeSelectLoading,
  makeSelectError,
  makeSelectEmail,
  makeSelectPassword,
} from './selectors';
import {
  userLoginJwt,
  userLogin,
  changeEmail,
  changePassword,
} from './actions';
import messages from './messages';

import reducer from './reducer';
import saga from './saga';

/* eslint-disable react/prefer-stateless-function */
export class LoginPage extends AbstractComponent {
  componentDidMount() {
    const { dispatchUserLoginJwt } = this.props;
    dispatchUserLoginJwt();
  }

  safeRender() {
    const {
      loading,
      // error,
      email,
      dispatchEmailChange,
      password,
      dispatchPasswordChange,
      dispatchUserLoginLocal,
      person,
    } = this.props;

    let content;

    if (person) {
      content = (
        <PageContainer
          title={messages.headerDone}
          titleValues={{ name: person.firstname }}
          description={messages.description}
        />
      );
    } else {
      content = (
        <PageContainer
          title={messages.headerLogin}
          description={messages.description}
        >
          <Form onSubmit={dispatchUserLoginLocal}>
            <Input
              name={messages.formEmailLabel}
              id="email"
              type="text"
              placeholder={messages.formEmailPlaceholder}
              value={email}
              loading={loading}
              onChange={dispatchEmailChange}
            />
            <Input
              name={messages.formPasswordLabel}
              id="password"
              type="password"
              placeholder={messages.formPasswordPlaceholder}
              value={password}
              loading={loading}
              onChange={dispatchPasswordChange}
            />
            <Button typeSubmit loading={loading}>
              <FormattedMessage {...messages.formLoginButton} />
            </Button>
          </Form>
        </PageContainer>
      );
    }

    return content;
  }
}

LoginPage.propTypes = {
  dispatchUserLoginJwt: PropTypes.func,
  dispatchUserLoginLocal: PropTypes.func,
  dispatchEmailChange: PropTypes.func,
  dispatchPasswordChange: PropTypes.func,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  email: PropTypes.string,
  password: PropTypes.string,
  person: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchUserLoginJwt: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(userLoginJwt());
    },
    dispatchEmailChange: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(changeEmail(evt.target.value));
    },
    dispatchPasswordChange: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(changePassword(evt.target.value));
    },
    dispatchUserLoginLocal: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(userLogin());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  email: makeSelectEmail(),
  password: makeSelectPassword(),
  person: makeSelectPerson(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(LoginPage);
