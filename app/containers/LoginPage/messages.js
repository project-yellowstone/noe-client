/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.LoginPage';

export default defineMessages({
  headerLogin: {
    id: `${scope}.header.login`,
    defaultMessage: 'Hello, please login',
  },
  headerDone: {
    id: `${scope}.header.done`,
    defaultMessage: 'Welcome {name}!',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Login page.',
  },
  formEmailLabel: {
    id: `${scope}.form.email.label`,
    defaultMessage: 'Email',
  },
  formEmailPlaceholder: {
    id: `${scope}.form.email.placeholder`,
    defaultMessage: 'my.id@mycompany.com',
  },
  formPasswordLabel: {
    id: `${scope}.form.password.label`,
    defaultMessage: 'Password',
  },
  formPasswordPlaceholder: {
    id: `${scope}.form.password.placeholder`,
    defaultMessage: 'My secret password',
  },
  formLoginButton: {
    id: `${scope}.form.button.login`,
    defaultMessage: 'Login',
  },
});
