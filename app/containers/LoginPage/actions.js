/*
 * Login actions
 *
 * Actions change things in your application. Since this boilerplate uses a uni-directional data flow, specifically redux, we have these actions which are the only way your application interacts with your application state. This guarantees that your state is up to date and nobody messes it up weirdly somewhere.
 */
import {
  USER_LOGIN_JWT,
  USER_LOGIN_JWT_OK,
  USER_LOGIN_JWT_ERROR,
  USER_LOGIN_LOCAL,
  USER_LOGIN_LOCAL_OK,
  USER_LOGIN_LOCAL_ERROR,
  USER_LOGOUT,
  USER_LOGOUT_OK,
  USER_LOGOUT_ERROR,
  PERSON_UPDATE,
  PERSON_UPDATE_OK,
  PERSON_UPDATE_ERROR,
  RESET_FORM,
  CHANGE_EMAIL,
  CHANGE_PASSWORD,
} from './constants';

export function userLoginJwt() {
  return {
    type: USER_LOGIN_JWT,
  };
}

export function userLoginJwtOk(user) {
  return {
    type: USER_LOGIN_JWT_OK,
    user,
  };
}

export function userLoginJwtError(error) {
  return {
    type: USER_LOGIN_JWT_ERROR,
    error,
  };
}

export function userLogin() {
  return {
    type: USER_LOGIN_LOCAL,
  };
}

export function userLoginOk(user) {
  return {
    type: USER_LOGIN_LOCAL_OK,
    user,
  };
}

export function userLoginError(error) {
  return {
    type: USER_LOGIN_LOCAL_ERROR,
    error,
  };
}

export function userLogout() {
  return {
    type: USER_LOGOUT,
  };
}

export function userLogoutError(error) {
  return {
    type: USER_LOGOUT_ERROR,
    error,
  };
}

export function userLogoutOk() {
  return {
    type: USER_LOGOUT_OK,
  };
}

export function personUpdate() {
  return {
    type: PERSON_UPDATE,
  };
}

export function personUpdateError(error) {
  return {
    type: PERSON_UPDATE_ERROR,
    error,
  };
}

export function personUpdateOk(person) {
  return {
    type: PERSON_UPDATE_OK,
    person,
  };
}

export function resetForm() {
  return {
    type: RESET_FORM,
  };
}

export function changeEmail(email) {
  return {
    type: CHANGE_EMAIL,
    email,
  };
}

export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}
