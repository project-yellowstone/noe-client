/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';

import { addNotification } from 'containers/Alerts/actions';
import api from 'utils/api';
import {
  userLoginJwtOk,
  userLoginJwtError,
  userLoginOk,
  userLoginError,
  personUpdate,
  personUpdateOk,
  personUpdateError,
  resetForm,
} from './actions';
import { USER_LOGIN_JWT, USER_LOGIN_LOCAL, PERSON_UPDATE } from './constants';
import {
  makeSelectUser,
  makeSelectEmail,
  makeSelectPassword,
} from './selectors';

export function* userLoginJwt() {
  try {
    // auth with jwt
    const userEntity = yield call(api.authenticateJwt);
    // store the user
    yield put(userLoginJwtOk(userEntity));
    yield put(personUpdate());
  } catch (err) {
    yield put(userLoginJwtError(err));
  }
}

export function* userLoginLocal() {
  const email = yield select(makeSelectEmail());
  const password = yield select(makeSelectPassword());

  try {
    // generate a new auth
    const userEntity = yield call(api.authenticateLocal, email, password);
    // store the user
    yield put(userLoginOk(userEntity));
    yield put(resetForm());
    yield put(personUpdate());
  } catch (err) {
    yield put(userLoginError(err));
    yield put(addNotification(err.message));
  }
}

export function* personUpdateWorker() {
  const user = yield select(makeSelectUser());

  try {
    // generate a new auth
    const personEntity = yield api.personsService.get(user.person);
    // store the user
    yield put(personUpdateOk(personEntity));
  } catch (err) {
    yield put(personUpdateError(err));
    yield put(addNotification(err.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* loginData() {
  // Watches for USER_LOGIN actions and calls getRepos when one comes in. By using `takeLatest` only the result of the latest API call is applied. It returns task descriptor (just like fork) so we can continue execution It will be cancelled automatically on component unmount
  yield takeLatest(USER_LOGIN_JWT, userLoginJwt);
  yield takeLatest(USER_LOGIN_LOCAL, userLoginLocal);
  yield takeLatest(PERSON_UPDATE, personUpdateWorker);
}
