/*
 * LoginPage recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';
import * as Sentry from '@sentry/browser';

import {
  USER_LOGIN_JWT,
  USER_LOGIN_JWT_OK,
  USER_LOGIN_JWT_ERROR,
  USER_LOGIN_LOCAL,
  USER_LOGIN_LOCAL_OK,
  USER_LOGIN_LOCAL_ERROR,
  USER_LOGOUT_OK,
  PERSON_UPDATE,
  PERSON_UPDATE_OK,
  PERSON_UPDATE_ERROR,
  RESET_FORM,
  CHANGE_EMAIL,
  CHANGE_PASSWORD,
} from './constants';

// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  email: '',
  password: '',
  userData: {
    user: false,
    person: false,
  },
};

function setSentryUser(user) {
  Sentry.configureScope(scope => {
    scope.setUser({
      id: user._id, // eslint-disable-line no-underscore-dangle
      email: user.email,
    });
  });
}

/* eslint-disable default-case, no-param-reassign */
const loginReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // USER_LOGIN_JWT
      case USER_LOGIN_JWT:
        setSentryUser(false);
        draft.userData.user = false;
        draft.loading = true;
        draft.error = false;
        break;

      case USER_LOGIN_JWT_OK:
        setSentryUser(action.user);
        draft.userData.user = action.user;
        draft.loading = false;
        break;

      case USER_LOGIN_JWT_ERROR:
        setSentryUser(false);
        draft.userData.user = false;
        draft.loading = false;
        draft.error = action.error;
        break;

      // USER_LOGIN_LOCAL
      case USER_LOGIN_LOCAL:
        setSentryUser(false);
        draft.userData.user = false;
        draft.loading = true;
        draft.error = false;
        break;

      case USER_LOGIN_LOCAL_OK:
        setSentryUser(action.user);
        draft.userData.user = action.user;
        draft.loading = false;
        break;

      case USER_LOGIN_LOCAL_ERROR:
        setSentryUser(false);
        draft.userData.user = false;
        draft.loading = false;
        draft.error = action.error;
        break;

      // USER_LOGOUT
      case USER_LOGOUT_OK:
        setSentryUser(false);
        draft.userData.user = false;
        draft.userData.person = false;
        break;

      // PERSON_UPDATE
      case PERSON_UPDATE:
        draft.userData.person = false;
        break;

      case PERSON_UPDATE_OK:
        draft.userData.person = action.person;
        break;

      case PERSON_UPDATE_ERROR:
        draft.userData.person = false;
        break;

      // RESET_FORM
      case RESET_FORM:
        draft.email = '';
        draft.password = '';
        break;

      // CHANGE_EMAIL
      case CHANGE_EMAIL:
        draft.email = action.email;
        break;

      // CHANGE_PASSWORD
      case CHANGE_PASSWORD:
        draft.password = action.password;
        break;
    }
  });

export default loginReducer;
