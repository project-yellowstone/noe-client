/*
 * LoginPage constants
 *
 * Each action has a corresponding type, which the reducer knows and picks up on. To avoid weird typos between the reducer and the actions, we save them as constants here. We prefix them with 'yourproject/YourComponent' so we avoid reducers accidentally picking up actions they shouldn't.
 */
export const USER_LOGIN_JWT = 'app/LoginPage/USER_LOGIN_JWT';
export const USER_LOGIN_JWT_OK = 'app/LoginPage/USER_LOGIN_JWT_OK';
export const USER_LOGIN_JWT_ERROR = 'app/LoginPage/USER_LOGIN_JWT_ERROR';

export const USER_LOGIN_LOCAL = 'app/LoginPage/USER_LOGIN_LOCAL';
export const USER_LOGIN_LOCAL_OK = 'app/LoginPage/USER_LOGIN_LOCAL_OK';
export const USER_LOGIN_LOCAL_ERROR = 'app/LoginPage/USER_LOGIN_LOCAL_ERROR';

export const USER_LOGOUT = 'app/LoginPage/USER_LOGOUT';
export const USER_LOGOUT_OK = 'app/LoginPage/USER_LOGOUT_OK';
export const USER_LOGOUT_ERROR = 'app/LoginPage/USER_LOGOUT_ERROR';

export const PERSON_UPDATE = 'app/App/PERSON_UPDATE';
export const PERSON_UPDATE_OK = 'app/App/PERSON_UPDATE_OK';
export const PERSON_UPDATE_ERROR = 'app/App/PERSON_UPDATE_ERROR';

export const RESET_FORM = 'app/LoginPage/RESET_FORM';
export const CHANGE_EMAIL = 'app/LoginPage/CHANGE_SEARCH';
export const CHANGE_PASSWORD = 'app/LoginPage/CHANGE_PASSWORD';
