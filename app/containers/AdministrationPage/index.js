import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import AbstractComponent from 'components/AbstractComponent';
import PageContainer from 'components/PageContainer';
import Users from './Users';
import Iam from './Iam';
import UserGroups from './UserGroups';
import { usersLoad, iamLoad, userGroupsLoad } from './actions';
import {
  makeSelectUsersData,
  makeSelectUsersLoading,
  makeSelectIamData,
  makeSelectIamLoading,
  makeSelectUserGroupsData,
  makeSelectUserGroupsLoading,
} from './selectors';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';

// eslint-disable react/prefer-stateless-function
export class AccountPage extends AbstractComponent {
  componentDidMount() {
    const {
      dispatchUsersLoad,
      dispatchUserGroupLoad,
      dispatchIamLoad,
    } = this.props;

    dispatchUsersLoad();
    dispatchUserGroupLoad();
    dispatchIamLoad();
  }

  safeRender() {
    const {
      dispatchUsersLoad,
      dispatchUserGroupLoad,
      dispatchIamLoad,
      users,
      usersLoading,
      userGroups,
      userGroupsLoading,
      iam,
      iamLoading,
    } = this.props;

    return (
      <PageContainer title={messages.header} description={messages.description}>
        <Users
          users={users}
          usersLoading={usersLoading}
          refresh={dispatchUsersLoad}
        />
        <UserGroups
          userGroups={userGroups}
          userGroupsLoading={userGroupsLoading}
          refresh={dispatchUserGroupLoad}
        />
        <Iam iam={iam} iamLoading={iamLoading} refresh={dispatchIamLoad} />
      </PageContainer>
    );
  }
}

AccountPage.propTypes = {
  dispatchUsersLoad: PropTypes.func,
  dispatchUserGroupLoad: PropTypes.func,
  dispatchIamLoad: PropTypes.func,
  users: PropTypes.array,
  usersLoading: PropTypes.bool,
  userGroups: PropTypes.array,
  userGroupsLoading: PropTypes.bool,
  iam: PropTypes.array,
  iamLoading: PropTypes.bool,
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchUsersLoad: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(usersLoad());
    },
    dispatchUserGroupLoad: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(userGroupsLoad());
    },
    dispatchIamLoad: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(iamLoad());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  users: makeSelectUsersData(),
  usersLoading: makeSelectUsersLoading(),
  userGroups: makeSelectUserGroupsData(),
  userGroupsLoading: makeSelectUserGroupsLoading(),
  iam: makeSelectIamData(),
  iamLoading: makeSelectIamLoading(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'administrationPage', reducer });
const withSaga = injectSaga({ key: 'administrationPage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AccountPage);
