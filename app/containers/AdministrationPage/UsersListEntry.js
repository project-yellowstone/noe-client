import React from 'react';
import PropTypes from 'prop-types';
import * as moment from 'moment/moment';

import AbstractComponent from 'components/AbstractComponent';
import TableEntry from 'components/TableEntry';

// eslint-disable react/prefer-stateless-function
export class UsersListEntry extends AbstractComponent {
  safeRender() {
    const { user } = this.props;

    return (
      <TableEntry
        values={[
          user.email,
          user.person.firstname,
          user.person.lastname,
          moment.default(user.createdAt).calendar(),
        ]}
      />
    );
  }
}

UsersListEntry.propTypes = {
  user: PropTypes.object,
};

export default UsersListEntry;
