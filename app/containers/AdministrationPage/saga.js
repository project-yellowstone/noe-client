import { put, takeLatest } from 'redux-saga/effects';

import api from 'utils/api';

import { addNotification } from 'containers/Alerts/actions';
import {
  usersLoadOk,
  usersLoadError,
  iamLoadOk,
  iamLoadError,
  userGroupsLoadOk,
  userGroupsLoadError,
} from './actions';
import { USERS_LOAD, IAM_LOAD, USERGROUPS_LOAD } from './constants';

export function* userLoad() {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.usersService.find({
      query: {
        $limit: 5,
        $populate: ['person'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    yield put(usersLoadOk(res));
  } catch (err) {
    yield put(usersLoadError(err));
    yield put(addNotification(err.message));
  }
}

export function* userGroupsLoad() {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.userGroupsService.find({
      query: {
        $limit: 5,
        $populate: ['permissions'],
        $sort: {
          updatedAt: -1,
        },
      },
    });
    yield put(userGroupsLoadOk(res));
  } catch (err) {
    yield put(userGroupsLoadError(err));
    yield put(addNotification(err.message));
  }
}

export function* iamLoad() {
  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.iamService.find({
      query: {
        $limit: 5,
        $sort: {
          updatedAt: -1,
        },
      },
    });
    yield put(iamLoadOk(res));
  } catch (err) {
    yield put(iamLoadError(err));
    yield put(addNotification(err.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* accountPageData() {
  // Watches for USER_LOGIN actions and calls getRepos when one comes in. By using `takeLatest` only the result of the latest API call is applied. It returns task descriptor (just like fork) so we can continue execution It will be cancelled automatically on component unmount
  yield takeLatest(USERS_LOAD, userLoad);
  yield takeLatest(USERGROUPS_LOAD, userGroupsLoad);
  yield takeLatest(IAM_LOAD, iamLoad);
}
