import React from 'react';
import PropTypes from 'prop-types';
import * as moment from 'moment/moment';

import AbstractComponent from 'components/AbstractComponent';
import TableEntry from 'components/TableEntry';

// eslint-disable react/prefer-stateless-function
export class IamListEntry extends AbstractComponent {
  safeRender() {
    const { iam } = this.props;

    return (
      <TableEntry
        values={[
          iam.name,
          iam.description,
          moment.default(iam.createdAt).calendar(),
        ]}
      />
    );
  }
}

IamListEntry.propTypes = {
  iam: PropTypes.object,
};

export default IamListEntry;
