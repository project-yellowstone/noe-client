import React from 'react';
import PropTypes from 'prop-types';

import AbstractComponent from 'components/AbstractComponent';
import { FormattedMessage } from 'react-intl';
import H3 from 'components/H3';
import UsersList from './UsersList';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class Users extends AbstractComponent {
  safeRender() {
    const { users, usersLoading, refresh } = this.props;

    return (
      <div>
        <H3>
          <FormattedMessage {...messages.usersListHeader} />
        </H3>
        <UsersList
          users={users}
          usersLoading={usersLoading}
          refresh={refresh}
        />
      </div>
    );
  }
}

Users.propTypes = {
  users: PropTypes.array,
  usersLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default Users;
