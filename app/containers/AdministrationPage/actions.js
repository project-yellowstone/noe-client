/*
 * AdministrationPage actions
 *
 * Actions change things in your application. Since this boilerplate uses a uni-directional data flow, specifically redux, we have these actions which are the only way your application interacts with your application state. This guarantees that your state is up to date and nobody messes it up weirdly somewhere.
 */
import {
  USERS_LOAD,
  USERS_LOAD_ERROR,
  USERS_LOAD_OK,
  USERGROUPS_LOAD,
  USERGROUPS_LOAD_OK,
  USERGROUPS_LOAD_ERROR,
  IAM_LOAD,
  IAM_LOAD_ERROR,
  IAM_LOAD_OK,
} from './constants';

export function usersLoad() {
  return {
    type: USERS_LOAD,
  };
}

export function usersLoadOk(users) {
  return {
    type: USERS_LOAD_OK,
    users,
  };
}

export function usersLoadError(error) {
  return {
    type: USERS_LOAD_ERROR,
    error,
  };
}

export function userGroupsLoad() {
  return {
    type: USERGROUPS_LOAD,
  };
}

export function userGroupsLoadOk(userGroups) {
  return {
    type: USERGROUPS_LOAD_OK,
    userGroups,
  };
}

export function userGroupsLoadError(error) {
  return {
    type: USERGROUPS_LOAD_ERROR,
    error,
  };
}

export function iamLoad() {
  return {
    type: IAM_LOAD,
  };
}

export function iamLoadOk(iam) {
  return {
    type: IAM_LOAD_OK,
    iam,
  };
}

export function iamLoadError(error) {
  return {
    type: IAM_LOAD_ERROR,
    error,
  };
}
