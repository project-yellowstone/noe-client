import React from 'react';
import PropTypes from 'prop-types';
import * as moment from 'moment/moment';

import AbstractComponent from 'components/AbstractComponent';
import TableEntry from 'components/TableEntry';

// eslint-disable react/prefer-stateless-function
export class UserGroupsListEntry extends AbstractComponent {
  safeRender() {
    const { userGroup } = this.props;

    return (
      <TableEntry
        values={[
          userGroup.name,
          userGroup.permissions.map(permission => permission.name).join(', '),
          moment.default(userGroup.createdAt).calendar(),
        ]}
      />
    );
  }
}

UserGroupsListEntry.propTypes = {
  userGroup: PropTypes.object,
};

export default UserGroupsListEntry;
