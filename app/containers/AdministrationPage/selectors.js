/**
 * AdministrationPage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAdministrationPage = state =>
  state.administrationPage || initialState;

const makeSelectUsersData = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.users.data,
  );

const makeSelectUsersLoading = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.users.loading,
  );

const makeSelectUsersError = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.users.error,
  );

const makeSelectUserGroupsData = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.userGroups.data,
  );

const makeSelectUserGroupsLoading = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.userGroups.loading,
  );

const makeSelectUserGroupsError = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.userGroups.error,
  );

const makeSelectIamData = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.iam.data,
  );

const makeSelectIamLoading = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.iam.loading,
  );

const makeSelectIamError = () =>
  createSelector(
    selectAdministrationPage,
    administrationPageState => administrationPageState.iam.error,
  );

export {
  selectAdministrationPage,
  makeSelectUsersData,
  makeSelectUsersLoading,
  makeSelectUsersError,
  makeSelectUserGroupsData,
  makeSelectUserGroupsLoading,
  makeSelectUserGroupsError,
  makeSelectIamData,
  makeSelectIamLoading,
  makeSelectIamError,
};
