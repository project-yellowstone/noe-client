import React from 'react';
import PropTypes from 'prop-types';

import AbstractComponent from 'components/AbstractComponent';
import { FormattedMessage } from 'react-intl';
import H3 from 'components/H3';
import UserGroupsList from './UserGroupsList';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class UserGroups extends AbstractComponent {
  safeRender() {
    const { userGroups, userGroupsLoading, refresh } = this.props;

    return (
      <div>
        <H3>
          <FormattedMessage {...messages.userGroupsListHeader} />
        </H3>
        <UserGroupsList
          userGroups={userGroups}
          userGroupsLoading={userGroupsLoading}
          refresh={refresh}
        />
      </div>
    );
  }
}

UserGroups.propTypes = {
  userGroups: PropTypes.array,
  userGroupsLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default UserGroups;
