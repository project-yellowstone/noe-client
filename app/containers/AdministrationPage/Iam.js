import React from 'react';
import PropTypes from 'prop-types';

import AbstractComponent from 'components/AbstractComponent';
import { FormattedMessage } from 'react-intl';
import H3 from 'components/H3';
import IamList from './IamList';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class Iam extends AbstractComponent {
  safeRender() {
    const { iam, iamLoading, refresh } = this.props;

    return (
      <div>
        <H3>
          <FormattedMessage {...messages.iamListHeader} />
        </H3>
        <IamList iam={iam} iamLoading={iamLoading} refresh={refresh} />
      </div>
    );
  }
}

Iam.propTypes = {
  iam: PropTypes.array,
  iamLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default Iam;
