/*
 * AccountPage recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';

import {
  USERS_LOAD,
  USERS_LOAD_ERROR,
  USERS_LOAD_OK,
  USERGROUPS_LOAD,
  USERGROUPS_LOAD_OK,
  USERGROUPS_LOAD_ERROR,
  IAM_LOAD,
  IAM_LOAD_ERROR,
  IAM_LOAD_OK,
} from './constants';

// The initial state of the App
export const initialState = {
  users: {
    data: [],
    loading: false,
    error: false,
  },
  userGroups: {
    data: [],
    loading: false,
    error: false,
  },
  iam: {
    data: [],
    loading: false,
    error: false,
  },
};

/* eslint-disable default-case, no-param-reassign */
const administrationPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // USERS_LOAD
      case USERS_LOAD:
        draft.users.loading = true;
        draft.users.error = false;
        break;

      case USERS_LOAD_OK:
        draft.users.loading = true;
        draft.users.data = action.users;
        break;

      case USERS_LOAD_ERROR:
        draft.users.loading = true;
        draft.users.error = true;
        break;

      // USERGROUPS_LOAD
      case USERGROUPS_LOAD:
        draft.userGroups.loading = true;
        draft.userGroups.error = false;
        break;

      case USERGROUPS_LOAD_OK:
        draft.userGroups.loading = true;
        draft.userGroups.data = action.userGroups;
        break;

      case USERGROUPS_LOAD_ERROR:
        draft.userGroups.loading = true;
        draft.userGroups.error = true;
        break;

      // IAM_LOAD
      case IAM_LOAD:
        draft.iam.loading = true;
        draft.iam.error = false;
        break;

      case IAM_LOAD_OK:
        draft.iam.loading = true;
        draft.iam.data = action.iam;
        break;

      case IAM_LOAD_ERROR:
        draft.iam.loading = true;
        draft.iam.error = true;
        break;
    }
  });

export default administrationPageReducer;
