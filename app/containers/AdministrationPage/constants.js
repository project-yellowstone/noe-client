/*
 * AdministrationPage constants
 *
 * Each action has a corresponding type, which the reducer knows and picks up on. To avoid weird typos between the reducer and the actions, we save them as constants here. We prefix them with 'yourproject/YourComponent' so we avoid reducers accidentally picking up actions they shouldn't.
 */
// USERS_LOAD
export const USERS_LOAD = 'app/AdministrationPage/USERS_LOAD';
export const USERS_LOAD_OK = 'app/AdministrationPage/USERS_LOAD_OK';
export const USERS_LOAD_ERROR = 'app/AdministrationPage/USERS_LOAD_ERROR';

// USERGROUPS_LOAD
export const USERGROUPS_LOAD = 'app/AdministrationPage/USERGROUPS_LOAD';
export const USERGROUPS_LOAD_OK = 'app/AdministrationPage/USERGROUPS_LOAD_OK';
export const USERGROUPS_LOAD_ERROR =
  'app/AdministrationPage/USERGROUPS_LOAD_ERROR';

// IAM_LOAD
export const IAM_LOAD = 'app/AdministrationPage/IAM_LOAD';
export const IAM_LOAD_OK = 'app/AdministrationPage/IAM_LOAD_OK';
export const IAM_LOAD_ERROR = 'app/AdministrationPage/IAM_LOAD_ERROR';
