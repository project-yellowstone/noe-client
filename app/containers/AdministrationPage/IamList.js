import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import A from 'components/A';
import Table from 'components/Table';
import IamListEntry from './IamListEntry';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class IamList extends AbstractComponent {
  safeRender() {
    const { iam, iamLoading, refresh: dispatchRefresh } = this.props;

    if (iam.length === 0 || iam.data.length === 0) return false;

    const iamList = iam.data.map(iamElement => (
      <IamListEntry key={iamElement._id} iam={iamElement} /> // eslint-disable-line no-underscore-dangle
    ));

    return (
      <div>
        <div>
          <Table
            head={[
              messages.iamListHeadName,
              messages.iamListHeadDescription,
              messages.iamListHeadCreateAt,
            ]}
            footer={[
              <FormattedMessage
                {...messages.listFooterDetails}
                values={{
                  amount: iam.total < iam.limit ? iam.total : iam.limit,
                  total: iam.total,
                }}
              />,
              <A onClick={dispatchRefresh}>
                <FormattedMessage {...messages.listFooterRefresh} />
              </A>,
            ]}
          >
            {iamList}
          </Table>
        </div>
        {iamLoading}
      </div>
    );
  }
}

IamList.propTypes = {
  iam: PropTypes.array,
  iamLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default IamList;
