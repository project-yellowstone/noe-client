import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import A from 'components/A';
import Table from 'components/Table';
import UsersListEntry from './UsersListEntry';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class UsersList extends AbstractComponent {
  safeRender() {
    const { users, usersLoading, refresh: dispatchRefresh } = this.props;

    if (users.length === 0 || users.data.length === 0) return false;

    const usersList = users.data.map(user => (
      <UsersListEntry key={user._id} user={user} /> // eslint-disable-line no-underscore-dangle
    ));

    return (
      <div>
        <div>
          <Table
            head={[
              messages.usersListHeadUsername,
              messages.usersListHeadFirstname,
              messages.usersListHeadLastname,
              messages.usersListHeadCreateAt,
            ]}
            footer={[
              <FormattedMessage
                {...messages.listFooterDetails}
                values={{
                  amount: users.total < users.limit ? users.total : users.limit,
                  total: users.total,
                }}
              />,
              <A onClick={dispatchRefresh}>
                <FormattedMessage {...messages.listFooterRefresh} />
              </A>,
            ]}
          >
            {usersList}
          </Table>
        </div>
        {usersLoading}
      </div>
    );
  }
}

UsersList.propTypes = {
  users: PropTypes.array,
  usersLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default UsersList;
