import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import A from 'components/A';
import Table from 'components/Table';
import UserGroupsListEntry from './UserGroupsListEntry';
import messages from './messages';

// eslint-disable react/prefer-stateless-function
export class UserGroupsList extends AbstractComponent {
  safeRender() {
    const {
      userGroups,
      userGroupsLoading,
      refresh: dispatchRefresh,
    } = this.props;

    if (userGroups.length === 0 || userGroups.data.length === 0) return false;

    const userGroupsList = userGroups.data.map(userGroup => (
      <UserGroupsListEntry key={userGroup._id} userGroup={userGroup} /> // eslint-disable-line no-underscore-dangle
    ));

    return (
      <div>
        <div>
          <Table
            head={[
              messages.userGroupsListHeadName,
              messages.userGroupsListHeadPermissions,
              messages.userGroupsListHeadCreateAt,
            ]}
            footer={[
              <FormattedMessage
                {...messages.listFooterDetails}
                values={{
                  amount:
                    userGroups.total < userGroups.limit
                      ? userGroups.total
                      : userGroups.limit,
                  total: userGroups.total,
                }}
              />,
              <A onClick={dispatchRefresh}>
                <FormattedMessage {...messages.listFooterRefresh} />
              </A>,
            ]}
          >
            {userGroupsList}
          </Table>
        </div>
        {userGroupsLoading}
      </div>
    );
  }
}

UserGroupsList.propTypes = {
  users: PropTypes.array,
  usersLoading: PropTypes.bool,
  refresh: PropTypes.func,
};

export default UserGroupsList;
