/*
 * AdministrationPage messages
 *
 * This contains all the text.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.LoginPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'Administration',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'Administration page',
  },
  listFooterDetails: {
    id: `${scope}.list.footer.details`,
    defaultMessage: 'Last {amount} from a total of {total}.',
  },
  listFooterRefresh: {
    id: `${scope}.list.footer.refresh`,
    defaultMessage: 'Refresh.',
  },
  usersListHeader: {
    id: `${scope}.users.list.header`,
    defaultMessage: 'Users',
  },
  usersListHeadUsername: {
    id: `${scope}.users.list.head.username`,
    defaultMessage: 'Username',
  },
  usersListHeadFirstname: {
    id: `${scope}.users.list.head.firstname`,
    defaultMessage: 'Firstname',
  },
  usersListHeadLastname: {
    id: `${scope}.users.list.head.lastname`,
    defaultMessage: 'Lastname',
  },
  usersListHeadCreateAt: {
    id: `${scope}.users.list.head.createAt`,
    defaultMessage: 'Registered from',
  },
  userGroupsListHeader: {
    id: `${scope}.userGroups.list.header`,
    defaultMessage: 'User groups',
  },
  userGroupsListHeadName: {
    id: `${scope}.userGroups.list.head.name`,
    defaultMessage: 'Name',
  },
  userGroupsListHeadPermissions: {
    id: `${scope}.userGroups.list.head.permissions`,
    defaultMessage: 'IAM list',
  },
  userGroupsListHeadCreateAt: {
    id: `${scope}.userGroups.list.head.createAt`,
    defaultMessage: 'Created at',
  },
  iamListHeader: {
    id: `${scope}.iam.list.header`,
    defaultMessage: 'IAM',
  },
  iamListHeadName: {
    id: `${scope}.iam.list.head.username`,
    defaultMessage: 'Name',
  },
  iamListHeadDescription: {
    id: `${scope}.iam.list.head.firstname`,
    defaultMessage: 'Description',
  },
  iamListHeadCreateAt: {
    id: `${scope}.iam.list.head.createAt`,
    defaultMessage: 'Created at',
  },
});
