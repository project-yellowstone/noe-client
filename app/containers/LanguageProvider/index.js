/*
 * LanguageProvider
 *
 * This component connects the redux state language locale to the IntlProvider component and i18n messages (Ok from `app/translations`)
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { IntlProvider } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import { makeSelectLocale } from './selectors';

export class LanguageProvider extends AbstractComponent {
  safeRender() {
    const { locale, messages, children } = this.props;

    return (
      <IntlProvider
        locale={locale}
        key={children}
        messages={messages[children]}
      >
        {React.Children.only(children)}
      </IntlProvider>
    );
  }
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

const mapStateToProps = createStructuredSelector({
  locale: makeSelectLocale(),
});

export default connect(mapStateToProps)(LanguageProvider);
