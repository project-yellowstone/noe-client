/*
 * LanguageProvider recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';
import * as Sentry from '@sentry/browser';

import { DEFAULT_LOCALE } from 'i18n';
import { CHANGE_LOCALE } from './constants';

export const initialState = {
  locale: DEFAULT_LOCALE,
};

/* eslint-disable default-case, no-param-reassign */
const languageProviderReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // CHANGE_LOCALE
      case CHANGE_LOCALE:
        Sentry.configureScope(scope => {
          scope.setTag('locale', action.locale);
        });
        draft.locale = action.locale;
        break;
    }
  });

export default languageProviderReducer;
