/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';

import PageContainer from 'components/PageContainer';
import messages from './messages';

export default function NotFound() {
  return (
    <PageContainer title={messages.header} description={messages.description}>
      <p>ABC</p>
    </PageContainer>
  );
}
