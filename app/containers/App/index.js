/**
 * App
 *
 * This component is the skeleton around the actual pages, and should only contain code that should be seen on all pages. (e.g. navigation bar)
 */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Switch, Route } from 'react-router-dom';

import AbstractComponent from 'components/AbstractComponent';
import HomePage from 'containers/HomePage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import AccountPage from 'containers/AccountPage/Loadable';
import AdministrationPage from 'containers/AdministrationPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Alerts from 'containers/Alerts';
import NavBar from 'components/NavBar';
import Footer from 'components/Footer';
import GlobalStyle from 'globalStyles';

import ContainerGlobal from './ContainerGlobal';
import ContainerGlobalInner from './ContainerGlobalInner';
import ContainerContent from './ContainerContent';
import ContainerContentInner from './ContainerContentInner';

/* eslint-disable react/prefer-stateless-function */
export class App extends AbstractComponent {
  safeRender() {
    return (
      <ContainerGlobal>
        <Helmet
          titleTemplate="%s - Noé project (MSIA18 - Yellowstone group)"
          defaultTitle="Noé project (MSIA18 - Yellowstone group)"
        >
          <meta
            name="description"
            content="Noé project, Yellowstone application, for the CESI Alternance school."
          />
        </Helmet>
        <ContainerGlobalInner>
          <NavBar />
          <Alerts />
          <ContainerContent>
            <ContainerContentInner>
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/account" component={AccountPage} />
                <Route exact path="/admin" component={AdministrationPage} />
                <Route path="" component={NotFoundPage} />
              </Switch>
            </ContainerContentInner>
            <Footer />
          </ContainerContent>
        </ContainerGlobalInner>
        <GlobalStyle />
      </ContainerGlobal>
    );
  }
}

App.propTypes = {
  dispatchUserLoginJwt: PropTypes.func,
};

export default App;
