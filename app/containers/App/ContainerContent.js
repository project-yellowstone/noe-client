import styled from 'styled-components';

export default styled.div`
  margin: 0 var(--scrollContainer-margins) var(--scrollContainer-margins)
    var(--scrollContainer-margins);
  border-radius: var(--radius);

  flex-grow: 1;
  background-color: var(--color-background-inner);
  color: var(--color-primary-text);
`;
