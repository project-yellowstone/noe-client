import styled from 'styled-components';

export default styled.div`
  max-width: var(--width-container);
  margin: var(--container-margin-v) auto;
  padding: 0 var(--container-margin-h);
`;
