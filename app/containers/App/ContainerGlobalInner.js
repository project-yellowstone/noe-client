import styled from 'styled-components';

export default styled.div`
  flex-grow: 1;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  background: var(--color-background);
`;
