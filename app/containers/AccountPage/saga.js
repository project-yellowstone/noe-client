import { call, put, select, takeLatest } from 'redux-saga/effects';

import api from 'utils/api';

import { addNotification } from 'containers/Alerts/actions';
import {
  userLogoutOk,
  userLogoutError,
  personUpdateOk,
  personUpdateError,
} from 'containers/LoginPage/actions';
import { USER_LOGOUT } from 'containers/LoginPage/constants';
import { makeSelectPerson } from 'containers/LoginPage/selectors';
import {
  firstnameUpdateError,
  firstnameUpdateOk,
  lastnameUpdateError,
  lastnameUpdateOk,
} from './actions';
import { makeSelectFirstname, makeSelectLastname } from './selectors';
import { FIRSTNAME_UPDATE, LASTNAME_UPDATE } from './constants';

/**
 * Logout handler
 */
export function* userLogout() {
  try {
    // ask to logout
    yield call(api.logout);
    // inform redux
    yield put(userLogoutOk());
  } catch (err) {
    yield put(userLogoutError(err));
    yield put(addNotification(err.message));
  }
}

/**
 * Update user firstname handler
 */
export function* personFirstnameUpdate() {
  const person = yield select(makeSelectPerson());
  const newFirstname = yield select(makeSelectFirstname());

  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.personsService.patch(person._id, {
      firstname: newFirstname,
    });
    const updatedPerson = {
      ...person,
      ...res,
    };
    yield put(firstnameUpdateOk());
    yield put(personUpdateOk(updatedPerson));
  } catch (err) {
    yield put(firstnameUpdateError(err));
    yield put(personUpdateError(err));
    yield put(addNotification(err.message));
  }
}

/**
 * Update user lastname handler
 */
export function* personLastnameUpdate() {
  const person = yield select(makeSelectPerson());
  const newLastname = yield select(makeSelectLastname());

  try {
    // eslint-disable-next-line no-underscore-dangle
    const res = yield api.personsService.patch(person._id, {
      lastname: newLastname,
    });
    const updatedPerson = {
      ...person,
      ...res,
    };
    yield put(lastnameUpdateOk());
    yield put(personUpdateOk(updatedPerson));
  } catch (err) {
    yield put(lastnameUpdateError(err));
    yield put(personUpdateError(err));
    yield put(addNotification(err.message));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* accountPageData() {
  // Watches for USER_LOGIN actions and calls getRepos when one comes in. By using `takeLatest` only the result of the latest API call is applied. It returns task descriptor (just like fork) so we can continue execution It will be cancelled automatically on component unmount
  yield takeLatest(USER_LOGOUT, userLogout);
  yield takeLatest(FIRSTNAME_UPDATE, personFirstnameUpdate);
  yield takeLatest(LASTNAME_UPDATE, personLastnameUpdate);
}
