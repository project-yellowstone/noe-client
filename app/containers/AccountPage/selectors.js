/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectAccount = state => state.account || initialState;

const makeSelectEmail = () =>
  createSelector(
    selectAccount,
    accountState => accountState.email,
  );

const makeSelectFirstname = () =>
  createSelector(
    selectAccount,
    accountState => accountState.firstname,
  );

const makeSelectFirstnameLoading = () =>
  createSelector(
    selectAccount,
    accountState => accountState.firstnameLoading,
  );

const makeSelectLastname = () =>
  createSelector(
    selectAccount,
    accountState => accountState.lastname,
  );

const makeSelectLastnameLoading = () =>
  createSelector(
    selectAccount,
    accountState => accountState.lastnameLoading,
  );

const makeSelectLogoutLoading = () =>
  createSelector(
    selectAccount,
    accountState => accountState.logoutLoading,
  );

const makeSelectLogoutError = () =>
  createSelector(
    selectAccount,
    accountState => accountState.logoutError,
  );

export {
  selectAccount,
  makeSelectEmail,
  makeSelectFirstname,
  makeSelectFirstnameLoading,
  makeSelectLastname,
  makeSelectLastnameLoading,
  makeSelectLogoutLoading,
  makeSelectLogoutError,
};
