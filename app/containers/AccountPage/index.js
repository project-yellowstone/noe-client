import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage } from 'react-intl';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import AbstractComponent from 'components/AbstractComponent';
import Button from 'components/Button';
import PageContainer from 'components/PageContainer';
import Form from 'components/Form';
import Input from 'components/Input';
import { makeSelectUser } from 'containers/LoginPage/selectors';
import { userLogout, personUpdate } from 'containers/LoginPage/actions';
import {
  firstnameType,
  lastnameType,
  firstnameUpdate,
  lastnameUpdate,
} from './actions';
import {
  makeSelectLogoutLoading,
  makeSelectLogoutError,
  makeSelectEmail,
  makeSelectFirstname,
  makeSelectFirstnameLoading,
  makeSelectLastname,
  makeSelectLastnameLoading,
} from './selectors';
import messages from './messages';
import reducer from './reducer';
import saga from './saga';

// eslint-disable react/prefer-stateless-function
export class AccountPage extends AbstractComponent {
  safeRender() {
    const {
      dispatchPersonUpdate,
      email,
      firstname,
      firstnameLoading,
      firstnameError,
      dispatchFirstnameType,
      dispatchFirstnameUpdate,
      lastname,
      lastnameLoading,
      lastnameError,
      dispatchLastnameType,
      dispatchLastnameUpdate,
      logoutLoading,
      logoutError,
      dispatchUserLogout,
    } = this.props;

    return (
      <PageContainer title={messages.header} description={messages.description}>
        <Form onSubmit={dispatchPersonUpdate}>
          <Input
            name={messages.formEmailLabel}
            id="email"
            type="text"
            value={email}
            disabled
          />
          <Input
            name={messages.formFirstnameLabel}
            id="firstname"
            type="text"
            placeholder={messages.formFirstnamePlaceholder}
            value={firstname}
            onChange={dispatchFirstnameType}
            onBlur={dispatchFirstnameUpdate}
            loading={firstnameLoading}
          />
          {firstnameError && <p>{firstnameError.message}</p>}
          <Input
            name={messages.formLastnameLabel}
            id="lastname"
            type="text"
            placeholder={messages.formLastnamePlaceholder}
            value={lastname}
            onChange={dispatchLastnameType}
            onBlur={dispatchLastnameUpdate}
            loading={lastnameLoading}
          />
          {lastnameError && <p>{lastnameError.message}</p>}
        </Form>
        <Button onClick={dispatchUserLogout} loading={logoutLoading}>
          <FormattedMessage {...messages.userLogoutButton} />
        </Button>
        {logoutError && <p>{logoutError.message}</p>}
      </PageContainer>
    );
  }
}

AccountPage.propTypes = {
  dispatchUserLogout: PropTypes.func,
  dispatchPersonUpdate: PropTypes.func,
  dispatchFirstnameType: PropTypes.func,
  dispatchFirstnameUpdate: PropTypes.func,
  dispatchLastnameType: PropTypes.func,
  dispatchLastnameUpdate: PropTypes.func,
  logoutLoading: PropTypes.bool,
  logoutError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  email: PropTypes.string,
  firstname: PropTypes.string,
  firstnameLoading: PropTypes.bool,
  firstnameError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  lastname: PropTypes.string,
  lastnameLoading: PropTypes.bool,
  lastnameError: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

export function mapDispatchToProps(dispatch) {
  return {
    dispatchPersonUpdate: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(personUpdate());
    },
    dispatchFirstnameType: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(firstnameType(evt.target.value));
    },
    dispatchFirstnameUpdate: () => {
      dispatch(firstnameUpdate());
    },
    dispatchLastnameType: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(lastnameType(evt.target.value));
    },
    dispatchLastnameUpdate: () => {
      dispatch(lastnameUpdate());
    },
    dispatchUserLogout: () => {
      dispatch(userLogout());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  email: makeSelectEmail(),
  user: makeSelectUser(),
  firstname: makeSelectFirstname(),
  firstnameLoading: makeSelectFirstnameLoading(),
  lastname: makeSelectLastname(),
  lastnameLoading: makeSelectLastnameLoading(),
  logoutLoading: makeSelectLogoutLoading(),
  logoutError: makeSelectLogoutError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'account', reducer });
const withSaga = injectSaga({ key: 'account', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AccountPage);
