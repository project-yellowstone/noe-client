/*
 * AccountPage actions
 *
 * Actions change things in your application. Since this boilerplate uses a uni-directional data flow, specifically redux, we have these actions which are the only way your application interacts with your application state. This guarantees that your state is up to date and nobody messes it up weirdly somewhere.
 */
import {
  FIRSTNAME_TYPE,
  LASTNAME_TYPE,
  FIRSTNAME_UPDATE,
  FIRSTNAME_UPDATE_ERROR,
  FIRSTNAME_UPDATE_OK,
  LASTNAME_UPDATE,
  LASTNAME_UPDATE_ERROR,
  LASTNAME_UPDATE_OK,
} from './constants';

export function firstnameType(firstname) {
  return {
    type: FIRSTNAME_TYPE,
    firstname,
  };
}

export function lastnameType(lastname) {
  return {
    type: LASTNAME_TYPE,
    lastname,
  };
}

export function firstnameUpdate() {
  return {
    type: FIRSTNAME_UPDATE,
  };
}

export function firstnameUpdateError(error) {
  return {
    type: FIRSTNAME_UPDATE_ERROR,
    error,
  };
}

export function firstnameUpdateOk() {
  return {
    type: FIRSTNAME_UPDATE_OK,
  };
}

export function lastnameUpdate() {
  return {
    type: LASTNAME_UPDATE,
  };
}

export function lastnameUpdateError(error) {
  return {
    type: LASTNAME_UPDATE_ERROR,
    error,
  };
}

export function lastnameUpdateOk() {
  return {
    type: LASTNAME_UPDATE_OK,
  };
}
