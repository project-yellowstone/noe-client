/*
 * AccountPage recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';

import {
  USER_LOGIN_JWT_OK,
  USER_LOGIN_LOCAL_OK,
  PERSON_UPDATE_OK,
  USER_LOGOUT,
  USER_LOGOUT_OK,
  USER_LOGOUT_ERROR,
} from 'containers/LoginPage/constants';
import {
  FIRSTNAME_TYPE,
  LASTNAME_TYPE,
  FIRSTNAME_UPDATE,
  FIRSTNAME_UPDATE_ERROR,
  FIRSTNAME_UPDATE_OK,
  LASTNAME_UPDATE,
  LASTNAME_UPDATE_ERROR,
  LASTNAME_UPDATE_OK,
} from './constants';

// The initial state of the App
export const initialState = {
  email: '',
  firstname: '',
  firstnameLoading: false,
  firstnameError: false,
  lastname: '',
  lastnameLoading: false,
  lastnameError: false,
  logoutLoading: false,
  logoutError: false,
};

/* eslint-disable default-case, no-param-reassign */
const accountReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // USER_LOGIN
      case USER_LOGIN_JWT_OK:
      case USER_LOGIN_LOCAL_OK:
        draft.email = action.user.email;
        break;

      // PERSON_UPDATE
      case PERSON_UPDATE_OK:
        draft.firstname = action.person.firstname;
        draft.lastname = action.person.lastname;
        break;

      // USER_LOGOUT
      case USER_LOGOUT:
        draft.logoutLoading = true;
        draft.logoutError = false;
        break;

      case USER_LOGOUT_OK:
        draft.logoutLoading = false;
        draft.email = '';
        draft.firstname = '';
        draft.lastname = '';
        break;

      case USER_LOGOUT_ERROR:
        draft.logoutLoading = false;
        draft.logoutError = action.error;
        break;

      // FIRSTNAME_TYPE
      case FIRSTNAME_TYPE:
        draft.firstname = action.firstname;
        break;

      // LASTNAME_TYPE
      case LASTNAME_TYPE:
        draft.lastname = action.lastname;
        break;

      // FIRSTNAME_UPDATE
      case FIRSTNAME_UPDATE:
        draft.firstnameLoading = true;
        draft.firstnameError = false;
        break;

      case FIRSTNAME_UPDATE_ERROR:
        draft.firstnameLoading = false;
        draft.firstnameError = action.error;
        break;

      case FIRSTNAME_UPDATE_OK:
        draft.firstnameLoading = false;
        draft.firstnameError = false;
        break;

      // LASTNAME_UPDATE
      case LASTNAME_UPDATE:
        draft.lastnameLoading = true;
        draft.lastnameError = false;
        break;

      case LASTNAME_UPDATE_ERROR:
        draft.lastnameLoading = false;
        draft.lastnameError = action.error;
        break;

      case LASTNAME_UPDATE_OK:
        draft.lastnameLoading = false;
        draft.lastnameError = false;
        break;
    }
  });

export default accountReducer;
