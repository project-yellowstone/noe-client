/*
 * AccountPage constants
 *
 * Each action has a corresponding type, which the reducer knows and picks up on. To avoid weird typos between the reducer and the actions, we save them as constants here. We prefix them with 'yourproject/YourComponent' so we avoid reducers accidentally picking up actions they shouldn't.
 */
export const FIRSTNAME_TYPE = 'app/AccountPage/FIRSTNAME_TYPE';

export const FIRSTNAME_UPDATE = 'app/AccountPage/FIRSTNAME_UPDATE';
export const FIRSTNAME_UPDATE_OK = 'app/AccountPage/FIRSTNAME_UPDATE_OK';
export const FIRSTNAME_UPDATE_ERROR = 'app/AccountPage/FIRSTNAME_UPDATE_ERROR';

export const LASTNAME_TYPE = 'app/AccountPage/LASTNAME_TYPE';

export const LASTNAME_UPDATE = 'app/AccountPage/LASTNAME_UPDATE';
export const LASTNAME_UPDATE_OK = 'app/AccountPage/LASTNAME_UPDATE_OK';
export const LASTNAME_UPDATE_ERROR = 'app/AccountPage/LASTNAME_UPDATE_ERROR';
