/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.LoginPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'My account',
  },
  description: {
    id: `${scope}.description`,
    defaultMessage: 'My account',
  },
  userLogoutButton: {
    id: `${scope}.button.userLogout`,
    defaultMessage: 'Logout',
  },
  formEmailLabel: {
    id: `${scope}.form.email.label`,
    defaultMessage: 'Email',
  },
  formFirstnameLabel: {
    id: `${scope}.form.firstname.label`,
    defaultMessage: 'Firstname',
  },
  formFirstnamePlaceholder: {
    id: `${scope}.form.firstname.placeholder`,
    defaultMessage: 'My firstname',
  },
  formLastnameLabel: {
    id: `${scope}.form.lastname.label`,
    defaultMessage: 'Lastname',
  },
  formLastnamePlaceholder: {
    id: `${scope}.form.lastname.placeholder`,
    defaultMessage: 'My lastname',
  },
});
