import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the themeToggle state domain
 */
const selectAlerts = state => state.alerts || initialState;

/**
 * Select the theme
 */
const makeSelectNotifications = () =>
  createSelector(
    selectAlerts,
    alertsState => alertsState.notifications,
  );

export { selectAlerts, makeSelectNotifications };
