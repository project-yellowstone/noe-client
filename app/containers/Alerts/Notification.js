/*
 * ThemeProvider
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { X } from 'react-feather';

import AbstractComponent from 'components/AbstractComponent';
import Container from './NotificationContainer';
import HideButton from './NotificationHideButton';
import { hideNotification } from './actions';

export class Alerts extends AbstractComponent {
  safeRender() {
    const { dispatchHide, content } = this.props;

    // auto-hide element
    setTimeout(dispatchHide, 7500);

    return (
      <Container>
        <p>{content}</p>
        <HideButton onClick={dispatchHide}>
          <X size={24} strokeWidth={2} />
        </HideButton>
      </Container>
    );
  }
}

Alerts.propTypes = {
  id: PropTypes.string,
  content: PropTypes.string,
};

export function mapDispatchToProps(dispatch, ownProps) {
  return {
    dispatchHide: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(hideNotification(ownProps.id));
    },
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(Alerts);
