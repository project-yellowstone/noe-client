/*
 * Alerts actions
 *
 * Actions change things in your application. Since this boilerplate uses a uni-directional data flow, specifically redux, we have these actions which are the only way your application interacts with your application state. This guarantees that your state is up to date and nobody messes it up weirdly somewhere.
 */
import { NOTIFICATION_ADD, NOTIFICATION_HIDE } from './constants';

export function addNotification(content) {
  return {
    type: NOTIFICATION_ADD,
    content,
  };
}

export function hideNotification(id) {
  return {
    type: NOTIFICATION_HIDE,
    id,
  };
}
