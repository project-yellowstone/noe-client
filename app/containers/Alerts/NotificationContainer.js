import styled from 'styled-components';

export default styled.section`
  @keyframes fade-in {
    from {
      transform: translateY(1rem);
      opacity: 0;
    }
    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  max-width: 450px;
  padding: 1rem;
  border-radius: var(--radius);
  background: var(--color-background);
  color: var(--color-background-text);
  animation: fade-in 400ms ease;
  box-shadow: 0 3px 6px rgba(var(--color-shadow), 0.16),
    0 3px 6px rgba(var(--color-shadow), 0.23);

  > :not(:last-child) {
    margin-right: 1rem;
  }
`;
