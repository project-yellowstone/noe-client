/*
 * ThemeProvider recuder
 *
 * The reducer takes care of our data. Using actions, we can change our application state. To add a new action, add it to the switch statement in the reducer function.
 */
import produce from 'immer';
import uuidv4 from 'uuid/v4';

import { NOTIFICATION_ADD, NOTIFICATION_HIDE } from './constants';

export const initialState = {
  notifications: {},
};

/* eslint-disable default-case, no-param-reassign */
const alertsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case NOTIFICATION_ADD:
        draft.notifications[uuidv4()] = {
          content: action.content,
        };
        break;

      case NOTIFICATION_HIDE:
        delete draft.notifications[action.id];
        break;
    }
  });

export default alertsReducer;
