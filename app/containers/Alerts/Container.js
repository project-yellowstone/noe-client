import styled from 'styled-components';

export default styled.span`
  position: fixed;
  bottom: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  z-index: 100;

  margin: 1rem;
`;
