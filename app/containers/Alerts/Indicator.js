import styled from 'styled-components';

export default styled.span`
  margin-bottom: 0.5rem;
  padding: 0.5rem;
  background: var(--color-primary);
  color: var(--color-primary-text);
  border-radius: 50%;
  line-height: 0.75rem;
  min-width: 0.75rem;
  text-align: center;
  font-size: 0.75rem;
  box-shadow: 0 3px 6px rgba(var(--color-shadow), 0.16),
    0 3px 6px rgba(var(--color-shadow), 0.23);
`;
