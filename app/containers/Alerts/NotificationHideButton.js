import styled from 'styled-components';

export default styled.span`
  display: flex;
  padding: 0;
  margin: 0;
  background: none;
  border: 0 none;
  color: inherit;
  cursor: pointer;
`;
