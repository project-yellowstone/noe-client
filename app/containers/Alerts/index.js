/*
 * ThemeProvider
 */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';
import AbstractComponent from 'components/AbstractComponent';
import Notification from './Notification';
import Container from './Container';
import Indicator from './Indicator';
import reducer from './reducer';
import { makeSelectNotifications } from './selectors';

export class Alerts extends AbstractComponent {
  safeRender() {
    const { notifications } = this.props;

    const notificationKeys = Object.keys(notifications);

    // if there is no notification, no render
    if (notificationKeys.length === 0) return false;

    /* eslint-disable react/no-array-index-key */
    const firstNotificationId = notificationKeys[0];
    const firstNotificationEntity = notifications[firstNotificationId];
    const firstNotification = (
      <Notification
        key={firstNotificationId}
        id={firstNotificationId}
        content={firstNotificationEntity.content}
      />
    );

    let indicator;
    if (notificationKeys.length > 1) {
      const text =
        notificationKeys.length < 10 ? notificationKeys.length - 1 : '9+';
      indicator = <Indicator>{text}</Indicator>;
    }

    return (
      <Container>
        {indicator}
        {firstNotification}
      </Container>
    );
  }
}

Alerts.propTypes = {
  notifications: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  notifications: makeSelectNotifications(),
});

const withConnect = connect(mapStateToProps);

const withReducer = injectReducer({ key: 'alerts', reducer });

export default compose(
  withReducer,
  withConnect,
)(Alerts);
