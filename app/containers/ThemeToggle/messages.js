/*
 * LocaleToggle Messages
 *
 * This contains all the text for the LanguageToggle component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'noe_server.containers.ThemeToggle';

export default defineMessages({
  label: {
    id: `${scope}.label`,
    defaultMessage: 'Theme',
  },
  white: {
    id: `${scope}.white`,
    defaultMessage: 'White',
  },
  dark: {
    id: `${scope}.dark`,
    defaultMessage: 'Dark',
  },
  black: {
    id: `${scope}.black`,
    defaultMessage: 'Black',
  },
});
