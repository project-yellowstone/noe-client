/*
 *
 * LanguageToggle
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';

import AbstractComponent from 'components/AbstractComponent';
import Toggle from 'components/Toggle';
import { appThemes } from 'themes';
import { changeTheme } from 'containers/ThemeProvider/actions';
import { makeSelectTheme } from 'containers/ThemeProvider/selectors';
import Wrapper from './Wrapper';
import messages from './messages';

export class ThemeToggle extends AbstractComponent {
  // eslint-disable-line react/prefer-stateless-function
  safeRender() {
    const { theme, onThemeToggle } = this.props;

    return (
      <Wrapper>
        <Toggle
          id="themeToogle"
          value={theme}
          values={appThemes}
          messages={messages}
          name={messages.label}
          onToggle={onThemeToggle}
        />
      </Wrapper>
    );
  }
}

ThemeToggle.propTypes = {
  onThemeToggle: PropTypes.func,
  theme: PropTypes.string,
};

const mapStateToProps = createSelector(
  makeSelectTheme(),
  theme => ({
    theme,
  }),
);

export function mapDispatchToProps(dispatch) {
  return {
    onThemeToggle: evt => dispatch(changeTheme(evt.target.value)),
    dispatch,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThemeToggle);
