import { createGlobalStyle } from 'styled-components';
import 'normalize.css';

import gothamLight from 'fonts/GothamLight.ttf';
import gothamLightItalic from 'fonts/GothamLightItalic.ttf';
import gothamMedium from 'fonts/GothamMedium.ttf';
import gothamMediumItalic from 'fonts/GothamMediumItalic.ttf';
import gothamBold from 'fonts/GothamBold.ttf';
import gothamBoldItalic from 'fonts/GothamBoldItalic.ttf';

const GlobalStyle = createGlobalStyle`
  /*
    Custom reset
  */
  p, h1, h2, h3, h4, h5, h6, tr, td, th {
    margin: unset; // default margins, found on Chromium
    text-align: unset;
    font-size: inherit;
  }

  /*
    App custom CSS
  */
  // Gotham font
  @font-face {
    font-family: 'Gotham';
    font-style: normal;
    font-weight: 300;
    src: local('Gotham Light'), local('Gotham-Light'), url(${gothamLight}) format('truetype');
  }
  @font-face {
    font-family: 'Gotham';
    font-style: italic;
    font-weight: 300;
    src: local('Gotham Light'), local('Gotham-Light'), url(${gothamLightItalic}) format('truetype');
  }
  @font-face {
    font-family: 'Gotham';
    font-style: normal;
    font-weight: 400;
    src: local('Gotham'), local('Gotham-Regular'), url(${gothamMedium}) format('truetype');
  }
  @font-face {
    font-family: 'Gotham';
    font-style: italic;
    font-weight: 400;
    src: local('Gotham'), local('Gotham-Regular'), url(${gothamMediumItalic}) format('truetype');
  }
  @font-face {
    font-family: 'Gotham';
    font-style: normal;
    font-weight: 700;
    src: local('Gotham Bold'), local('Gotham-Bold'), url(${gothamBold}) format('truetype');
  }
  @font-face {
    font-family: 'Gotham';
    font-style: italic;
    font-weight: 700;
    src: local('Gotham Bold'), local('Gotham-Bold'), url(${gothamBoldItalic}) format('truetype');
  }

  :root {
    --radius: 3px;
    --width-container: 1284px;
    --width-container-small: 912px;
    --scrollContainer-margins: .5rem;

    font-size: 16px;

    @media only screen and (min-width: 912px) {
      --container-margin-h: 2rem;
      --container-margin-v: 4rem;
    }

    @media only screen and (max-width: 912px) and (min-width: 476px) {
      --container-margin-h: 1rem;
      --container-margin-v: 2rem;
    }

    @media only screen and (max-width: 476px) {
      --container-margin-h: .5rem;
      --container-margin-v: 2rem;
    }
  }

  hmtl,
  body,
  #app {
    min-height: 100vh;
  }

  body {
    font-family: Verdana, Geneva, "DejaVu Sans", sans-serif;

    &.fontLoadedRoboto {
      font-family: 'Roboto', Verdana, Geneva, "DejaVu Sans", sans-serif;
    }
  }

  b,
  strong {
    font-weight: 700;
  }

  i,
  em {
    font-style: italic;
  }
`;

export default GlobalStyle;
