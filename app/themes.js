/**
 * Themes
 */
// prettier-ignore
const appThemes = [
  'white',
  'dark',
  'black',
];

const DEFAULT_THEME = appThemes[1];

const themeStyles = {
  // https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=bcbcbc&secondary.color=4DB6AC
  white: {
    '--color-primary': '#bdbdbd',
    '--color-primary-light': '#efefef',
    '--color-primary-dark': '#8d8d8d',
    '--color-primary-text': 'rgba(0, 0, 0, .87)',
    '--color-primary-text-light': 'rgba(0, 0, 0, .6)',
    '--color-secondary': '#4db6ac',
    '--color-secondary-light': '#82e9de',
    '--color-secondary-dark': '#00867d',
    '--color-secondary-text': 'rgba(0, 0, 0, .87)',
    '--color-secondary-text-light': 'rgba(0, 0, 0, .6)',
    '--color-background-inner': '#fff',
    '--color-background': '#000',
    '--color-background-text': 'rgba(255, 255, 255, .87)',
    '--color-shadow': '141, 141, 141',
  },
  // https://material.io/tools/color/#!/?view.left=1&view.right=0&primary.color=1b1b1b&secondary.color=283593
  dark: {
    '--color-primary': '#263238',
    '--color-primary-light': '#000',
    '--color-primary-dark': '#4f5b62',
    '--color-primary-text': 'rgba(255, 255, 255, .87)',
    '--color-primary-text-light': 'rgba(255, 255, 255, .6)',
    '--color-secondary': '#004d40',
    '--color-secondary-light': '#00251a',
    '--color-secondary-dark': '#39796b',
    '--color-secondary-text': 'rgba(255, 255, 255, .87)',
    '--color-secondary-text-light': 'rgba(255, 255, 255, .6)',
    '--color-background-inner': '#1b1b1b',
    '--color-background': '#000',
    '--color-background-text': 'rgba(255, 255, 255, .87)',
    '--color-shadow': '0, 0, 0',
  },
  // https://material.io/tools/color/#!/?view.left=1&view.right=0&primary.color=212121&secondary.color=1A237E
  black: {
    '--color-primary': '#212121',
    '--color-primary-light': '#000',
    '--color-primary-dark': '#484848',
    '--color-primary-text': 'rgba(255, 255, 255, .87)',
    '--color-primary-text-light': 'rgba(255, 255, 255, .6)',
    '--color-secondary': '#004d40',
    '--color-secondary-light': '#39796b',
    '--color-secondary-dark': '#00251a',
    '--color-secondary-text': 'rgba(255, 255, 255, .87)',
    '--color-secondary-text-light': 'rgba(255, 255, 255, .6)',
    '--color-background-inner': 'var(--color-primary-light)',
    '--color-background': '#000',
    '--color-background-text': 'rgba(255, 255, 255, .87)',
    '--color-shadow': '255, 255, 255',
  },
};

exports.themeStyles = themeStyles;
exports.appThemes = appThemes;
exports.DEFAULT_THEME = DEFAULT_THEME;
