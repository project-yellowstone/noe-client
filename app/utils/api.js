import config from 'config';
import io from 'socket.io-client';
import Feathers from '@feathersjs/client';

// Socket.io is exposed as the `io` global.
const socket = io(config.apiRootUrl, {
  transports: ['websocket'],
  upgrade: false,
});

// @feathersjs/client is exposed as the `feathers` global.
const feathers = Feathers();

feathers.configure(Feathers.socketio(socket));
feathers.configure(
  Feathers.authentication({
    storage: window.localStorage,
    path: '/authentication',
    jwtStrategy: 'jwt',
    storageKey: 'feathers-jwt',
  }),
);

const iamService = feathers.service('iam');
const personsService = feathers.service('persons');
const phylogeneticService = feathers.service('phylogenetic');
const projectsService = feathers.service('projects');
const resourcesService = feathers.service('resources');
const userGroupsService = feathers.service('userGroups');
const usersService = feathers.service('users');
const warehousesService = feathers.service('warehouses');

function authenticateJwt() {
  return new Promise((resolve, reject) => {
    feathers
      .authenticate()
      .then(response => {
        if (response.errors) reject(response.errors);
        resolve(response.userData);
      })
      .catch(reject);
  });
}

function authenticateLocal(email, password) {
  return new Promise((resolve, reject) => {
    feathers
      .authenticate({
        strategy: 'local',
        email,
        password,
      })
      .then(response => {
        if (response.errors) reject(response.errors);
        resolve(response.userData);
      })
      .catch(reject);
  });
}

function logout() {
  return new Promise((resolve, reject) => {
    feathers
      .logout()
      .then(() => {
        resolve();
      })
      .catch(reject);
  });
}

export default {
  authenticateJwt,
  authenticateLocal,
  logout,
  iamService,
  personsService,
  phylogeneticService,
  projectsService,
  resourcesService,
  userGroupsService,
  usersService,
  warehousesService,
};
