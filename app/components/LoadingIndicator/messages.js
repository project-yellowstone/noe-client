/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.LoadingIndocator';

export default defineMessages({
  details: {
    id: `${scope}.details`,
    defaultMessage: 'Fetching data from space',
  },
});
