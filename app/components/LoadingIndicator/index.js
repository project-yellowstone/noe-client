import React from 'react';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';
import Loader from 'components/Loader';

import messages from './messages';

const Wrapper = styled.div`
  margin: 4rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;

  > :not(:last-child) {
    margin-bottom: 0.25rem;
  }
`;

const LoadingIndicator = () => (
  <Wrapper>
    <Loader />
    <FormattedMessage {...messages.details} />
  </Wrapper>
);

export default LoadingIndicator;
