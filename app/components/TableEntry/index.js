/*
 * Input
 */
import React from 'react';
import PropTypes from 'prop-types';

import AbstractComponent from 'components/AbstractComponent';
import BodyEntry from './BodyEntry';
import BodyLine from './BodyLine';

/* eslint-disable react/prefer-stateless-function */
export class TableEntry extends AbstractComponent {
  safeRender() {
    const { values } = this.props;

    const valueComponent = values.map(value => <BodyEntry>{value}</BodyEntry>);

    return <BodyLine>{valueComponent}</BodyLine>;
  }
}

TableEntry.propTypes = {
  values: PropTypes.array,
};

export default TableEntry;
