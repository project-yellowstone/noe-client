import styled from 'styled-components';

export default styled.td`
  text-align: left;
  flex-basis: 100%;
  min-width: 0;
  word-break: break-word;
  overflow: hidden;
  box-sizing: content-box;

  @media only screen and (min-width: 912px) {
    margin: 1rem;
  }

  @media only screen and (max-width: 912px) {
    margin: 0.4rem;
  }
`;
