import styled from 'styled-components';

export default styled.tr`
  display: flex;

  align-items: center;
  border-style: solid;
  border-color: var(--color-primary-light);
  border-width: 0 0 1px 0;

  &:not(:hover) {
    td {
      max-height: 3rem;
    }
  }

  &:hover {
    background: var(--color-primary-light);
  }
`;
