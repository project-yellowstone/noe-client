import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { injectIntl, intlShape } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import H2 from 'components/H2';

/* eslint-disable react/prefer-stateless-function */
export class AccountPage extends AbstractComponent {
  safeRender() {
    const {
      intl,
      title: titleRef,
      titleValues,
      description: descriptionRef,
      descriptionValues,
      children,
    } = this.props;

    const title = intl.formatMessage(titleRef, titleValues);
    const description = intl.formatMessage(descriptionRef, descriptionValues);

    return (
      <section>
        <Helmet>
          <title>{title}</title>
          <meta name="description" content={description} />
        </Helmet>
        <header>
          <H2>{title}</H2>
        </header>
        {Children.toArray(children)}
      </section>
    );
  }
}

AccountPage.propTypes = {
  intl: intlShape.isRequired,
  title: PropTypes.object.isRequired,
  titleValues: PropTypes.object,
  description: PropTypes.object.isRequired,
  descriptionValues: PropTypes.object,
  children: PropTypes.node,
};

export default injectIntl(AccountPage);
