import styled from 'styled-components';

export default styled.select`
  border: 0 none;
  background: none;
  box-sizing: content-box;
  cursor: inherit;

  font-weight: 700;
  padding: 1rem;
  min-height: 1.5rem;
  color: var(--color-primary-text);

  &:active,
  &:focus {
    background: var(--color-primary-light);
  }
`;
