/*
 * Toggle
 */
import React from 'react';
import PropTypes from 'prop-types';

import Label from 'components/Label';
import AbstractComponent from 'components/AbstractComponent';
import Select from './Select';
import ToggleOption from '../ToggleOption';

/* eslint-disable react/prefer-stateless-function */
export class Toggle extends AbstractComponent {
  safeRender() {
    const {
      id,
      value,
      values,
      messages,
      name: nameRef,
      onToggle,
      disabled,
      loading,
    } = this.props;

    const { active } = this.state;

    let content = <option>--</option>;

    // If we have items, render them
    if (values) {
      content = values.map(valuesValue => (
        <ToggleOption
          key={valuesValue}
          value={valuesValue}
          message={messages[valuesValue]}
        />
      ));
    }

    return (
      <Label
        id={id}
        name={nameRef}
        active={active}
        disabled={disabled}
        loading={loading}
        clickable
        standardBehaviour
      >
        <Select
          id={id}
          value={value}
          disabled={disabled}
          onChange={onToggle}
          onFocus={() => {
            this.setState({ active: true });
          }}
          onBlur={() => {
            this.setState({ active: false });
          }}
        >
          {content}
        </Select>
      </Label>
    );
  }
}

Toggle.propTypes = {
  id: PropTypes.string.isRequired,
  onToggle: PropTypes.func.isRequired,
  name: PropTypes.object.isRequired,
  messages: PropTypes.object,
  values: PropTypes.array,
  value: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
};

export default Toggle;
