import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Activity } from 'react-feather';

import * as Sentry from '@sentry/browser';
import A from 'components/A';
import messages from './messages';
import Wrapper from './Wrapper';
import Icon from './Icon';

class AbstractComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error });

    Sentry.withScope(scope => {
      Object.keys(errorInfo).forEach(key => {
        scope.setExtra(key, errorInfo[key]);
      });
      Sentry.captureException(error);
    });
  }

  render() {
    const { error } = this.state;

    if (error) {
      // render fallback UI
      return (
        <Wrapper>
          <Icon>
            <Activity
              size={64}
              strokeWidth={1}
              color="var(--color-secondary)"
            />
          </Icon>
          <FormattedMessage {...messages.errorTitle} />
          <A onClick={() => Sentry.showReportDialog()}>
            <FormattedMessage {...messages.errorSubmit} />
          </A>
        </Wrapper>
      );
    }

    // when there's not an error, render children
    const childRender = this.safeRender();
    return childRender || null;
  }

  safeRender() {
    const { children } = this.props;

    return Children.toArray(children);
  }
}

AbstractComponent.propTypes = {
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  children: PropTypes.node,
};

export default AbstractComponent;
