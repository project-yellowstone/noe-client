/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.AbstractComponent';

export default defineMessages({
  errorTitle: {
    id: `${scope}.error.title`,
    defaultMessage: 'Something weird appended',
  },
  errorSubmit: {
    id: `${scope}.error.submit`,
    defaultMessage: 'If you want to report this error, please click here',
  },
});
