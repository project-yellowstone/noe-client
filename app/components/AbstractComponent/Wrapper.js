import styled from 'styled-components';

export default styled.div`
  margin: 4rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;

  > :not(:last-child) {
    margin-bottom: 0.25rem;
  }
`;
