import styled from 'styled-components';

const H2 = styled.h2`
  font-family: Gotham;
  font-size: 1.5rem;
  font-weight: 400;
  margin: 3rem 0;
`;

export default H2;
