/**
 * A link to a certain page, an anchor tag
 */

import styled from 'styled-components';

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

export default Form;
