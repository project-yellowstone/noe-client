import styled from 'styled-components';

export default styled.footer`
  display: flex;
  margin: 0 auto;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;

  padding: calc(var(--container-margin-v) / 2) var(--container-margin-h);
  max-width: var(--width-container);
  border-width: 2px 0 0 0;
  border-color: var(--color-primary-light);
  border-style: solid;

  > :not(:last-child) {
    margin-right: 1rem;
  }
`;
