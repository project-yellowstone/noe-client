/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Footer';

export default defineMessages({
  license: {
    id: `${scope}.license`,
    defaultMessage: 'This project is licensed under the MIT license.',
  },
  projectName: {
    id: `${scope}.project_name`,
    defaultMessage: `
      Made with love by the Yellowstone team.
    `,
  },
});
