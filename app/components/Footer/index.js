import React from 'react';
import { FormattedMessage } from 'react-intl';

import LocaleToggle from 'containers/LocaleToggle';
import ThemeToggle from 'containers/ThemeToggle';
import Wrapper from './Wrapper';
import messages from './messages';

function Footer() {
  return (
    <Wrapper>
      <section>
        <FormattedMessage {...messages.license} />
      </section>
      <section>
        <LocaleToggle />
      </section>
      <section>
        <ThemeToggle />
      </section>
      <section>
        <FormattedMessage {...messages.projectName} />
      </section>
    </Wrapper>
  );
}

export default Footer;
