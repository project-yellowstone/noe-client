import styled from 'styled-components';

// This come from https://github.com/MatejKustec/SpinThatShit.
const Loader = styled.div`
  @keyframes loader-scale {
    0% {
      transform: scale(0);
      opacity: 0;
    }

    50% {
      opacity: 1;
    }

    100% {
      transform: scale(1);
      opacity: 0;
    }
  }

  width: ${props => props.size || '4rem'};
  height: ${props => props.size || '4rem'};
  border: 0.2rem solid var(${props => props.color || '--color-secondary'});
  border-radius: 50%;
  position: relative;
  animation: loader-scale 1s ease-out infinite;
`;

export default Loader;
