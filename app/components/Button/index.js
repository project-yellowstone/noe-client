/**
 * Button
 */
import React, { Children } from 'react';
import PropTypes from 'prop-types';

import Loader from 'components/Loader';
import A from './A';
import StyledButton from './StyledButton';
import Wrapper from './Wrapper';

function Button(props) {
  const { href, loading, onClick, children, typeSubmit, handleRoute } = props;

  let button;

  // If the Button has a handleRoute prop, we want to render a button
  if (handleRoute || typeSubmit) {
    button = (
      <StyledButton onClick={handleRoute} loading={loading}>
        {loading && <Loader color="--color-primary" size="2rem" />}
        {Children.toArray(children)}
      </StyledButton>
    );
  } else {
    // Render an anchor tag
    button = (
      <A href={href || '#'} onClick={onClick} loading={loading}>
        {loading && <Loader color="--color-primary" size="2rem" />}
        {Children.toArray(children)}
      </A>
    );
  }

  return <Wrapper>{button}</Wrapper>;
}

Button.propTypes = {
  loading: PropTypes.bool,
  handleRoute: PropTypes.func,
  href: PropTypes.string,
  onClick: PropTypes.func,
  typeSubmit: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

export default Button;
