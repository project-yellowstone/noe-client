import { css } from 'styled-components';

const buttonStyles = css`
  text-decoration: none;
  user-select: none;
  border: 0 none;

  display: inline-flex;
  align-items: center;
  justify-content: center;

  font-size: 0.9rem;
  background: var(--color-secondary);
  color: var(--color-secondary-text);
  letter-spacing: 0.04em;
  line-height: 3em;
  min-width: 4rem;
  text-transform: uppercase;
  font-weight: 700;
  border-radius: var(--radius);
  padding: 0 1rem;
  filter: saturate(1);

  > :not(:last-child) {
    margin-right: 1em;
  }

  &:hover {
    transition: box-shadow 200ms ease-out;
    box-shadow: 0 3px 6px rgba(var(--color-shadow), 0.16),
      0 3px 6px rgba(var(--color-shadow), 0.23);
  }

  &:active {
    filter: saturate(0.6);
    transition: filter 200ms ease-out;
  }

  &:focus {
    &:not(:active) {
      text-decoration: underline;
    }
  }

  ${props =>
    !props.loading &&
    css`
      cursor: pointer;
    `};

  ${props =>
    props.loading &&
    css`
      cursor: progress;
    `};
`;

export default buttonStyles;
