import styled from 'styled-components';

export default styled.div`
  width: 100%;
  text-align: center;
  margin: 2rem 0;
`;
