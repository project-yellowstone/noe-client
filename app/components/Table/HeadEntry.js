import styled from 'styled-components';

export default styled.th`
  margin: 0;
  border: 0 none;

  flex-basis: 100%;
  min-width: 0;
  font-weight: 700;
  word-break: break-word;

  @media only screen and (min-width: 912px) {
    margin: 1rem;
  }

  @media only screen and (max-width: 912px) {
    margin: 0.4rem;
  }
`;
