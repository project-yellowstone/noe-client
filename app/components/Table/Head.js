import styled from 'styled-components';

export default styled.thead`
  border-style: solid;
  border-color: var(--color-primary);
  border-width: 1px 0;
`;
