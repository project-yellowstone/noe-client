import styled from 'styled-components';

export default styled.footer`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  font-size: 0.8rem;

  @media only screen and (min-width: 912px) {
    margin: 1rem 1rem 0 1rem;

    > :not(:last-child) {
      margin-right: 0.5rem;
    }
  }

  @media only screen and (max-width: 912px) {
    margin: 0.4rem 0.4rem 0 0.4rem;

    > :not(:last-child) {
      margin-right: 0.2rem;
    }
  }
`;
