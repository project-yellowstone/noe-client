import styled from 'styled-components';

export default styled.tr`
  display: flex;
  align-items: center;
`;
