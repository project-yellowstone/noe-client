/*
 * Input
 */
import React, { Children } from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';

import AbstractComponent from 'components/AbstractComponent';
import Container from './Container';
import Footer from './Footer';
import Table from './Table';
import Head from './Head';
import HeadEntry from './HeadEntry';
import HeadLine from './HeadLine';
import Body from './Body';

/* eslint-disable react/prefer-stateless-function */
export class TableComponent extends AbstractComponent {
  safeRender() {
    const { intl, head, footer: footerChildrens, children } = this.props;

    const headComponents = head.map(headValue => {
      const name = intl.formatMessage(headValue);
      return <HeadEntry>{name}</HeadEntry>;
    });

    let footer;
    if (footerChildrens) {
      footer = <Footer>{Children.toArray(footerChildrens)}</Footer>;
    }

    return (
      <Container>
        <Table>
          <Head>
            <HeadLine>{headComponents}</HeadLine>
          </Head>
          <Body>{Children.toArray(children)}</Body>
        </Table>
        {footer}
      </Container>
    );
  }
}

TableComponent.propTypes = {
  intl: intlShape.isRequired,
  head: PropTypes.array,
  footer: PropTypes.node,
  children: PropTypes.node,
};

export default injectIntl(TableComponent);
