/*
 * Input
 */
import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl, intlShape } from 'react-intl';

import Label from 'components/Label';
import AbstractComponent from 'components/AbstractComponent';
import InputField from './InputField';

/* eslint-disable react/prefer-stateless-function */
export class Input extends AbstractComponent {
  componentDidMount() {
    const { value } = this.props;

    // initiate the fill property
    this.setState({ ...this.getState, fill: value.length > 0 });
  }

  safeRender() {
    const {
      intl,
      id,
      disabled: disabledTmp,
      loading,
      type,
      name: nameRef,
      placeholder: placeholderRef,
      value,
      onChange: dispatchChange,
      onBlur: dispatchBlur,
    } = this.props;

    const { active: activeTmp, fill } = this.state;

    const disabled = disabledTmp || loading;
    const active = (!disabled && fill) || activeTmp;
    const placeholder = placeholderRef
      ? intl.formatMessage(placeholderRef)
      : null;

    return (
      <Label
        id={id}
        name={nameRef}
        active={active}
        loading={loading}
        disabled={disabled}
        onBlur={evt => {
          this.setState({ ...this.getState, active: false });
          if (dispatchBlur) dispatchBlur(evt);
        }}
        onFocus={() => {
          this.setState({ ...this.getState, active: true });
        }}
      >
        <InputField
          id={id}
          type={type}
          placeholder={placeholder}
          value={value}
          disabled={disabled}
          onChange={evt => {
            this.setState({
              ...this.getState,
              fill: evt.target.value.length > 0,
            });
            if (dispatchChange) dispatchChange(evt);
          }}
        />
      </Label>
    );
  }
}

Input.propTypes = {
  intl: intlShape.isRequired,
  id: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  type: PropTypes.string.isRequired,
  name: PropTypes.object.isRequired,
  placeholder: PropTypes.object,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
};

export default injectIntl(Input);
