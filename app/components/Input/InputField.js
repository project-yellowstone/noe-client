import styled from 'styled-components';

export default styled.input`
  border: 0 none;
  background: none;
  flex-grow: 1;

  cursor: inherit;
  padding: 1rem;
  line-height: 1.5rem;
  font-weight: 700;
  color: var(--color-primary-text);

  ::placeholder {
    font-weight: 400;
  }
`;
