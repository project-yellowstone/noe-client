/**
 * A link to a certain page, an anchor tag
 */
import styled from 'styled-components';

const A = styled.a`
  cursor: pointer;
  color: var(--color-secondary-dark);

  &:hover,
  &:active {
    text-decoration: underline;
  }
`;

export default A;
