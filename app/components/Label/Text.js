import styled, { css } from 'styled-components';

export default styled.span`
  position: absolute;
  margin: 0 1rem;
  line-height: 1rem;

  border-radius: var(--radius);
  padding: 0.25rem;

  ${props =>
    !props.active &&
    css`
      color: var(--color-primary-text);
    `};

  ${props =>
    (props.active || props.disabled) &&
    css`
      color: var(--color-secondary-text);
      transition-duration: 200ms;
    `};

  ${props =>
    !props.standardBehaviour &&
    css`
      transition-timing-function: ease-out;
      transition-property: transform, font-size, background;
      transition-duration: 100ms;
    `};

  ${props =>
    props.standardBehaviour &&
    css`
      transform: translateY(calc(-1.75rem - 2px)) translateX(-0.25rem);
      background: var(--color-primary);
      font-size: 0.8rem;

      transition-timing-function: ease-out;
      transition-property: background;
      transition-duration: 100ms;
    `};

  ${props =>
    (props.active || props.disabled) &&
    !props.standardBehaviour &&
    css`
      transform: translateY(calc(-1.75rem - 2px)) translateX(-0.25rem);
      font-size: 0.8rem;
    `};

  ${props =>
    props.active &&
    css`
      background: var(--color-secondary);
    `};

  ${props =>
    props.disabled &&
    css`
      background: var(--color-primary-light);
    `};
`;
