import styled, { css } from 'styled-components';

export default styled.label`
  display: flex;
  overflow: visible;
  position: relative;
  align-items: stretch;
  flex-direction: column;
  justify-content: center;

  margin: 1rem 0;
  color: var(--color-secondary-text);
  border-radius: var(--radius);
  border: 1px solid var(--color-primary-dark);

  ${props =>
    (props.disabled || props.loading) &&
    css`
      user-select: none;
      border-color: var(--color-primary);
    `};

  ${props =>
    props.disabled &&
    !props.loading &&
    css`
      cursor: not-allowed;
    `};

  ${props =>
    props.disabled &&
    props.loading &&
    css`
      cursor: progress;
    `};

  ${props =>
    !props.disabled &&
    !props.loading &&
    !props.clickable &&
    css`
      cursor: text;
    `};

  ${props =>
    !props.disabled &&
    !props.loading &&
    props.clickable &&
    css`
      cursor: pointer;
    `};

  ${props =>
    props.active &&
    css`
      border-color: var(--color-secondary);
      box-shadow: 0 0 0 1px var(--color-secondary);
    `};
`;
