import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Container from './Container';
import ContainerInner from './ContainerInner';
import Text from './Text';

function Label(props) {
  const {
    id,
    name: nameRef,
    children,
    disabled,
    loading,
    active,
    clickable,
    standardBehaviour,
    onBlur,
    onFocus,
  } = props;

  return (
    <Container
      htmlFor={id}
      disabled={disabled}
      loading={loading}
      active={active}
      clickable={clickable}
      standardBehaviour={standardBehaviour}
      onBlur={onBlur}
      onFocus={onFocus}
    >
      <Text
        active={active}
        standardBehaviour={standardBehaviour}
        disabled={disabled}
      >
        <FormattedMessage {...nameRef} />
      </Text>
      <ContainerInner standardBehaviour={standardBehaviour} active={active}>
        {children}
      </ContainerInner>
    </Container>
  );
}

Label.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  name: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  active: PropTypes.bool,
  clickable: PropTypes.bool,
  standardBehaviour: PropTypes.bool,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
};

export default Label;
