import styled, { css } from 'styled-components';

export default styled.span`
  display: flex;
  flex-direction: row;

  margin: 2px;

  *::placeholder {
    color: transparent;
    transition: color 100ms ease-out;
  }

  ${props =>
    props.active &&
    css`
      *::placeholder {
        color: var(--color-primary-text-light);
        transition: color 200ms ease-out;
      }
    `};
`;
