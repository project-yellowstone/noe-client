import styled from 'styled-components';

const H1 = styled.h1`
  font-family: Gotham;
  font-weight: 400;
  font-size: 2rem;
  margin: 4rem 0;
`;

export default H1;
