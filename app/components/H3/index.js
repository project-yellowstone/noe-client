import styled from 'styled-components';

const H3 = styled.h3`
  font-family: Gotham;
  font-size: 1.2rem;
  font-weight: 400;
  margin: 2.4rem 0;
`;

export default H3;
