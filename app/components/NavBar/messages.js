/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.components.Header';

export default defineMessages({
  navHomeTitle: {
    id: `${scope}.nav.home.title`,
    defaultMessage: 'Home',
  },
  navLoginTitle: {
    id: `${scope}.nav.login.title`,
    defaultMessage: 'Login',
  },
  navAdministrationTitle: {
    id: `${scope}.nav.administration.title`,
    defaultMessage: 'Administration',
  },
  navAccountTitle: {
    id: `${scope}.nav.account.title`,
    defaultMessage: 'My account',
  },
  navAccountImageAlt: {
    id: `${scope}.nav.account.imageAlt`,
    defaultMessage: 'Profile picture of {name}.',
  },
});
