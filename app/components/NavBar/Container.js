import styled from 'styled-components';

export default styled.header`
  --navbar-background: var(--color-primary-dark);
  --navbar-action-margins: 5px;

  z-index: 200;

  padding: 0 var(--scrollContainer-margins);
  background: var(--color-background);

  @media only screen and (min-width: 912px) {
    position: sticky;
    top: 0;
  }
`;
