import H1 from 'components/H1';
import styled from 'styled-components';

export default styled(H1)`
  display: flex;
  flex-wrap: 0;
  align-items: center;
  text-decoration: none;
  position: sticky;
  top: 0;
  margin: unset;

  text-align: left;
  line-height: 1;
  font-family: Gotham;
  color: var(--color-background-text);
  background: var(--color-background);

  > * {
    color: inherit;
    text-decoration: inherit;

    padding: 0.5rem var(--container-margin-h) 0.5rem 0;

    &:focus {
      &:not(:active) {
        text-decoration: underline;
      }
    }
  }

  @media only screen and (min-width: 912px) {
    margin: 0 auto 0 0;
  }

  @media only screen and (max-width: 912px) {
    justify-content: center;
    flex-basis: 100vw;
    order: unset;
  }
`;
