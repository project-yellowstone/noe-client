import styled from 'styled-components';

export default styled.header`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  flex-wrap: no-wrap;
  max-width: var(--width-container);
  margin: 0 auto;

  @media only screen and (min-width: 912px) {
    padding: 0 var(--container-margin-h);
  }

  @media only screen and (max-width: 912px) {
    flex-wrap: wrap;
  }
`;
