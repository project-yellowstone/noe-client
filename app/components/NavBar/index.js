import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';
import { Home, LogIn } from 'react-feather';
import { Link } from 'react-router-dom';

import AbstractComponent from 'components/AbstractComponent';
import uuid from 'uuid/v4';
import {
  makeSelectUser,
  makeSelectPerson,
} from 'containers/LoginPage/selectors';
import ContainerInner from './ContainerInner';
import Container from './Container';
import Logo from './Logo';
import Action from './Action';
import ActionIcon from './ActionIcon';
import ActionImage from './ActionImage';
import messages from './messages';

/* eslint-disable react/prefer-stateless-function */
class NavBar extends AbstractComponent {
  safeRender() {
    const { intl, user, person } = this.props;

    const userAction = [];

    if (user && person) {
      // user is connected
      userAction.push(
        <Action key={uuid()} to="/admin">
          <FormattedMessage {...messages.navAdministrationTitle} />
        </Action>,
      );

      userAction.push(
        <Action key={uuid()} to="/account">
          <ActionImage
            src={person.avatar}
            alt={intl.formatMessage(messages.navAccountImageAlt, {
              name: person.firstname,
            })}
          />
          <FormattedMessage {...messages.navAccountTitle} />
        </Action>,
      );
    } else {
      // used is disconnected
      userAction.push(
        <Action key={uuid()} to="/login">
          <ActionIcon>
            <LogIn
              size={24}
              strokeWidth={1}
              color="var(--color-primary-text)"
            />
          </ActionIcon>
          <FormattedMessage {...messages.navLoginTitle} />
        </Action>,
      );
    }

    return (
      <Container>
        <ContainerInner>
          <Logo>
            <Link to="/">Yellowstone</Link>
          </Logo>
          <Action to="/">
            <ActionIcon>
              <Home
                size={24}
                strokeWidth={1}
                color="var(--color-primary-text)"
              />
            </ActionIcon>
            <FormattedMessage {...messages.navHomeTitle} />
          </Action>
          {userAction}
        </ContainerInner>
      </Container>
    );
  }
}

NavBar.propTypes = {
  intl: intlShape.isRequired,
  user: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  person: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  person: makeSelectPerson(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  injectIntl,
  withConnect,
)(NavBar);
