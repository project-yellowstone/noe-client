import { Link } from 'react-router-dom';
import styled from 'styled-components';

export default styled(Link)`
  display: flex;
  box-sizing: border-box;
  flex-wrap: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  user-select: none;
  cursor: pointer;
  color: var(--color-primary-text);
  font-size: 1rem;
  line-height: 1;
  flex-basis: 14rem;
  background: var(--color-primary);
  border-radius: var(--radius);

  &:hover {
    text-decoration: underline;
  }

  &:focus {
    &:not(:active) {
      text-decoration: underline;
    }
  }

  @media only screen and (min-width: 912px) {
    margin: 0 0 var(--navbar-action-margins) var(--navbar-action-margins);
    margin-top: var(--navbar-action-margins);
    padding: 0.5rem var(--container-margin-h);
  }

  @media only screen and (max-width: 912px) and (min-width: 476px) {
    flex-basis: calc(50% - var(--navbar-action-margins) / 2);

    &:nth-child(2n) {
      margin: 0 calc(var(--navbar-action-margins) / 2)
        var(--navbar-action-margins) 0;
    }

    &:nth-child(2n + 1) {
      margin: 0 0 var(--navbar-action-margins)
        calc(var(--navbar-action-margins) / 2);
    }
  }

  @media only screen and (max-width: 912px) {
    padding: 1rem var(--container-margin-h);
  }

  @media only screen and (max-width: 476px) {
    flex-basis: 100vw;
    margin: 0 0 var(--navbar-action-margins) 0;
  }
`;
