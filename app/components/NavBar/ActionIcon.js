import styled from 'styled-components';

export default styled.span`
  color: transparent;
  margin-right: 0.5rem;
`;
