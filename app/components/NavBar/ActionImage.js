import styled from 'styled-components';

export default styled.img`
  height: 1.5rem;
  width: auto;
  margin-right: 0.5rem;
  border-radius: var(--radius);
`;
